<?php
/**
 * Created by 'PhpStorm.
 * User: souzajbr
 * Date: 06/08/18
 * Time: 10:01
 */

class ProcessApiClient
{

    protected $baseUrl;

    protected function textviewUrl()
    {
        return $this->baseUrl . '/textview';
    }

    protected function metricsUrl()
    {
        return $this->baseUrl . '/metricas';
    }

    protected function timelineUrl()
    {
        return $this->baseUrl . '/timeline';
    }

    public function __construct($url)
    {
        $this->baseUrl = $url;
    }

    /**
     * @param string $url
     * @param string $audioPath
     * @param int $projectId
     * @return string
     */
    public static function process($url, $audioPath, $projectId)
    {
        $request = curl_init($url);

        curl_setopt($request, CURLOPT_VERBOSE, false);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, array('file' => new CURLFile(realpath($audioPath)), 'project' => $projectId));
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($request);

        curl_close($request);

        return $response;
    }

    public function processTimeline($audio, $projectId)
    {
        return self::process($this->timelineUrl(), $audio, $projectId);
    }

    public function processMetrics($audio, $projectId)
    {
        return self::process($this->metricsUrl(), $audio, $projectId);
    }

    public function processTextview($audio, $projectId) {
        return self::process($this->textviewUrl(), $audio, $projectId);
    }
}
