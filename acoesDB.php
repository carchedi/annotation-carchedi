<?php 
	include_once 'includes/db_connect.inc.php';
	include_once 'includes/functions.inc.php';
    session_start(); 

    function organizaDatas($string){
    	$datas = explode(" - ", $string);
    	$inicio = explode("/",$datas[0]);
		$fim = explode("/",$datas[1]); 

		$date1 = $inicio[2]."-".$inicio[1]."-".$inicio[0];
		$date2 = $fim[2]."-".$fim[1]."-".$fim[0];

		$result[0] = $date1;
		$result[1] = $date2;

		return $result;
    }   

    if (isset($_POST['operacaoH'])) { 

	    //CRIAR UM NOVO PROJETO
	    if ($_POST['operacaoH'] == 1) {    
		//criaProjeto($_POST['nomeProjeto'],$_POST['descricaoProjeto'],$_POST['inicio'],$_POST['termino'],$_SESSION['NOME'],$_POST['idExterno'],$mysqli); 
	    	$result = organizaDatas($_POST['daterange']);

	    	echo "Nome: ".$_POST['nomeProjeto']."<br>";
	    	echo "Descricao: ".$_POST['descricaoProjeto']."<br>";
	    	echo "Inicio: ".$result[0]."<br>";
	    	echo "Fim: ".$result[1]."<br>";

	    	echo "IdExterno: ".$_POST['idExterno']."<br>";
		}

		//FINALIZAR UM PROJETO
		if ($_POST['operacaoH'] == 2) {
			finalizaProjeto($_POST['nomeH'],$mysqli); 
		}

		//EDITAR OS DADOS DE UM PROJETO
		if ($_POST['operacaoH'] == 3) {  
			$datas = organizaDatas($_POST['periodoH']);
			editaProjeto($_POST['idProjetoH'],$_POST['nomeH'],$_POST['descricaoH'],$datas[0],$datas[1],$mysqli);   
		} 

	}
 	//header("Location: {$_SERVER['HTTP_REFERER']}");
?>