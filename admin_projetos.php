<html>
    <?php

    	function gera_data(){
    		$date = rand(10,28)."/0".rand(1,6)."/2018";
    		return $date;
    	}


        session_start();
        //Access POST variables
        if( isset($_POST['projeto'])){
            $projeto = $_POST['projeto'];
            echo $projeto;
        }
                
        //Access variables in session
        $nome = $_SESSION['NOME'];
        $senha = $_SESSION['SENHA'];

        if ($nome == null) {
            header('Location: index.html');
        }
    ?>   
    <head>
	 	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/sidebar.css"> 
        <link rel="stylesheet" href="css/table.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

        <!--datepicker--> 
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

        <!--Gráfico-->
        <script src="https://cdn.plot.ly/plotly-latest.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/numeric/1.2.6/numeric.min.js"></script>

        <script type="text/javascript">
            function showPicker(valor){ 
                if (valor == 1) {
                    document.getElementById("daterange").style.visibility = "visible";
                }else{
                    document.getElementById("daterange").style.visibility = "hidden";
                }
            } 
        </script>
    </head>
	<body style="background-color: rgb(256,256,256);">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="navbar-brand">Projetos</div>
                </div>
                <ul class="nav navbar-nav navbar-right"> 
                    <li><a href="logout.php">Sair</a></li>
                </ul>
            </div>
        </nav>   
        <div style="padding-top: 50px;">
            <div id="wrapper" style="background-color: white">
                <!-- Sidebar -->
                <div id="sidebar-wrapper">
                    <nav id="spy">
                        <ul class="sidebar-nav nav">                     
                           <li>
                                <a href="admin_visaoGeral.php" data-scroll>
                                    <span class="fa fa-anchor solo">Visão Geral</span>
                                </a>
                            </li>
                            <li>
                                <a href="admin_projetos.php" data-scroll>
                                    <span class="fa fa-anchor solo">Projetos</span>
                                </a>
                            </li> 
                        </ul>
                    </nav>
                </div>

                <!-- Page content -->
                <div id="page-content-wrapper" style="padding-top: 10px;">
                    <div style="padding: 10px;">
                        <center>
	                        <h3 style="font-size:23px">Bem-vindo <?php echo $nome;?></h3>
	                        <hr style="width: 550px; margin-top: 0px; border: 0; bottom:0px; border-top: 2px solid #204d74;"/>
	                        <h4>Projetos</h4>  
							<select class="form-control" id="periodo" style="width: 250px" onchange="showPicker(this.value)">
                                <option value="0">Desde o início</option>
                                <option value="1">Período:</option>
                            </select> 
 
                            <input type="text" style="width: 250px; margin-top: 10px; visibility: hidden;" name="daterange" id='daterange' class="form-control"/>
                            <script>
                                $(function(){
                                  $('input[name="daterange"]').daterangepicker({
                                      autoUpdateInput: false,
                                      locale: {
                                          cancelLabel: 'Clear'
                                      }
                                  });
                                  $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
                                        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                                        alert("Mostrar apenas os projetos entre "+picker.startDate.format('DD/MM/YYYY')+" e "+picker.endDate.format('DD/MM//YYYY'));
                                    }
                                  );
                                  $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
                                      $(this).val('');
                                    }
                                  );
                                });
                            </script>
                        </center>     
                    </div>   

                    <div class="col-sm-6" style="margin: auto; padding:0px;">   
	                        <div id="myDiv2"><!-- Plotly chart will be drawn inside this DIV -->
	                          <script> 
								   var trace1 = {
                              x: [48,14,27], 
                              y: ["Satisfatório","Insatisfatório","Indefinido"], 
                              marker:{
                                color: ['rgba(14,14,230,0.8)',
                                        'rgba(230,14,14,0.8)',
                                        'rgba(204,204,204,1)']
                              },  
  							  orientation: 'h',
                              type: 'bar'
                            }; 
                            var data = [trace1]; 
                            var layout = { 
                                autosize: false,
                                width: 500,
                                height: 300,  
                                margin: {
								    l: 90,
								    r: 50,
								    b: 30,
								    t: 100,
								    pad: 0
								},
                                title: 'Processados'};
                            Plotly.newPlot('myDiv2', data, layout);
	                          </script> 
	                    </div> 
                    </div> 

                    <div class="col-sm-6" style="margin-top: 5px; padding:0;">   
	                        <div id="myDiv3"><!-- Plotly chart will be drawn inside this DIV -->
	                          <script> 
							var trace1 = {
                              x: [68,21], 
                              y: ["Satisfatório","Insatisfatório"], 
                              marker:{
                                color: ['rgba(14,14,230,0.8)',
                                        'rgba(230,14,14,0.8)']
                              },  
  							  orientation: 'h',
                              type: 'bar'
                            }; 
                            var data = [trace1]; 
                            var layout = { 
                                autosize: false,
                                width: 500,
                                height: 300,
                                margin: {
								    l: 90,
								    r: 50,
								    b: 60,
								    t: 100,
								    pad: 0
								},
                                title: 'Avaliados'};
                            Plotly.newPlot('myDiv3', data, layout);
	                          </script> 
	                    </div> 
                    </div>   

                    <div class="col-sm-12" style="margin: auto;">   
                        <div class="table-responsive">  
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Projeto</th>
                                    <th>Criador</th>
                                    <th>Início</th>
                                    <th>Término</th>
                                    <th>Acessos</th>
                                    <th>Detalhes</th>
                                </tr>
                                </thead>
                                <tbody>
                                 <?php
                                    for ($i=0; $i < 500 ; $i++) {    
                                        echo "<tr><td>Projeto_0".$i."</td><td>Admin</td><td>".gera_data()."</td><td>".gera_data()."</td><td>".rand(0,1000)."</td><td><button class=\"btn btn-info btn\" type=\"button\">
                                        <span class=\"glyphicon glyphicon-search\"></span> 
                                    </button> </td></tr>";
                                    }
                                ?>
                                </tbody>
                            </table> 
                        </div>           
                    </div>

                </div> 
            </div>
        </div> 
    </body> 
</html>
