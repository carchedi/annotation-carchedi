<html>
    <?php
        session_start();
        //Access POST variables
        if( isset($_POST['projeto'])){
            $projeto = $_POST['projeto'];
            echo $projeto;
        }
                
        //Access variables in session
        $nome = $_SESSION['NOME'];
        $senha = $_SESSION['SENHA'];

        if ($nome == null) {
            header('Location: index.html');
        }
    ?>   
    <head>
	 	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/sidebar.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

        <!--datepicker--> 
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

        <!--Gráfico-->
        <script src="https://cdn.plot.ly/plotly-latest.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/numeric/1.2.6/numeric.min.js"></script>
        <script type="text/javascript">
            function showPicker(valor){ 
                if (valor == 1) {
                    document.getElementById("daterange").style.visibility = "visible";
                }else{
                    document.getElementById("daterange").style.visibility = "hidden";
                }
            } 
        </script>
    </head>
    <body style="background-color: rgb(256,256,256);">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="navbar-brand">Visão Geral</div>
                </div>
                <ul class="nav navbar-nav navbar-right"> 
                    <li><a href="logout.php">Sair</a></li>
                </ul>
            </div>
        </nav>   
        <div style="padding-top: 50px;">
            <div id="wrapper" style="background-color: white">
                <!-- Sidebar -->
                <div id="sidebar-wrapper">
                    <nav id="spy">
                        <ul class="sidebar-nav nav">                     
                            <li>
                                <a href="admin_visaoGeral.php" data-scroll>
                                    <span class="fa fa-anchor solo">Visão Geral</span>
                                </a>
                            </li>
                            <li>
                                <a href="admin_projetos.php" data-scroll>
                                    <span class="fa fa-anchor solo">Projetos</span>
                                </a> 
                            </li>
                        </ul>
                    </nav>
                </div>

                <!-- Page content -->
                <div id="page-content-wrapper" style="padding-top: 10px;">
                    <div style="padding: 10px;">
                        <center>
                        <h3 style="font-size:23px">Bem-vindo <?php echo $nome;?></h3>
                        <hr style="width: 550px; margin-top: 0px; border: 0; bottom:0px; border-top: 2px solid #204d74;"/>
                        <h4>Visão Geral do Sistema</h4>  
                            <select class="form-control" id="periodo" style="width: 250px" onchange="showPicker(this.value)">
                                <option value="0">Desde o início</option>
                                <option value="1">Período:</option>
                            </select> 
 
                            <input type="text" style="width: 250px; margin-top: 10px; visibility: hidden;" name="daterange" id='daterange' class="form-control"/>
                            <script>
                                $(function(){
                                  $('input[name="daterange"]').daterangepicker({
                                      autoUpdateInput: false,
                                      locale: {
                                          cancelLabel: 'Clear'
                                      }
                                  });
                                  $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
                                        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                                        alert("Mostrar apenas os projetos entre "+picker.startDate.format('DD/MM/YYYY')+" e "+picker.endDate.format('DD/MM//YYYY'));
                                    }
                                  );
                                  $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
                                      $(this).val('');
                                    }
                                  );
                                });
                            </script>

                        </center>     
                    </div>
                    <div class="col-sm-2" style="margin: auto;"> 
                    </div>
                    <div class="col-sm-4" style="margin: auto;"></div>
                    <div class="col-sm-6" style="margin: auto;">  
                        <div id="myDiv"><!-- Plotly chart will be drawn inside this DIV -->
                          <script>
                            var trace1 = {
                              x: [1, 2, 3, 4, 5, 6, 7, 8, 9], 
                              y: [10, 15, 13, 17, 21, 12, 9, 14, 17],
                              type: 'bar'
                            }; 
                            var data = [trace1]; 
                            var layout = { 
                                autosize: false,
                                width: 500,
                                height: 300,
                                title: 'Acessos'};
                            Plotly.newPlot('myDiv', data, layout);
                          </script> 
                        </div>
                    </div>  

                    <div class="col-sm-6" style="margin: auto;"> 
                        <div id="myDiv2"><!-- Plotly chart will be drawn inside this DIV -->
                          <script> 
                            var data = [{
                                        values: [9, 22],
                                        labels: ['Finalizados','Em andamento'], 
                                        type: 'pie'
                                       }]; 
                            var layout = {
                                autosize: false,
                                width: 500,
                                height: 300,
                                title: 'Projetos'};
                            Plotly.newPlot('myDiv2', data, layout);
                          </script>
                        </div>
                    </div> 

                    <div class="col-sm-6" style="margin: auto;"> 
                        <div id="myDiv3"><!-- Plotly chart will be drawn inside this DIV -->
                          <script>
                            var trace1 = {
                              values:[10, 15, 13, 3],
                              labels: ['Premium','Ouro','Ativo', 'Inativo'], 
                              type: 'pie'
                            }; 
                            var data = [trace1]; 
                            var layout = { 
                                autosize: false,
                                width: 500,
                                height: 300,
                                title: 'Colaboradores'};
                            Plotly.newPlot('myDiv3', data, layout);
                          </script>
                        </div>
                    </div>                    
                </div> 
            </div>
        </div>
    </body> 
</html>
