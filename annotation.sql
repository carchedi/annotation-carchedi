-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 08-Jun-2018 às 16:27
-- Versão do servidor: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `annotation`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `audios`
--

CREATE TABLE `audios` (
  `idAudio` int(11) NOT NULL,
  `caminho` varchar(200) DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `resultado` int(11) NOT NULL,
  `qtdErro` int(11) NOT NULL,
  `ultimaPalavra` int(11) NOT NULL,
  `pausaSentido` int(11) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `idProjeto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `audios`
--

INSERT INTO `audios` (`idAudio`, `caminho`, `estado`, `resultado`, `qtdErro`, `ultimaPalavra`, `pausaSentido`, `usuario`, `idProjeto`) VALUES
(1, 'audios/output/ruim/16_51400000080128-D030003H6.wav', 2, 1, 33, 33, 1, 'user', 69594),
(2, 'audios/output/ruim/16_51500000250119-D030003H6.wav', 2, 1, 335, 335, 1, 'user2', 69594);

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentarios`
--

CREATE TABLE `comentarios` (
  `idComentario` int(11) NOT NULL,
  `comentario` text,
  `inicio` float NOT NULL,
  `idTipo` int(11) NOT NULL,
  `fim` float NOT NULL,
  `cor` varchar(50) NOT NULL,
  `idAudio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `comentarios`
--

INSERT INTO `comentarios` (`idComentario`, `comentario`, `inicio`, `idTipo`, `fim`, `cor`, `idAudio`) VALUES
(5, 'Quarto comentário', 0, 0, 10, 'rgba(0,0,255,0.4)', 2),
(22, 'Primeiro comentário', 10.9, 0, 24, 'rgba(26, 129, 255, 0.6)', 1),
(23, 'Segundo comentário', 7.7, 0, 14.2, 'rgba(26, 129, 255, 0.6)', 1),
(24, 'Terceiro comentário', 17.6, 0, 26.5, 'rgba(26, 129, 255, 0.6)', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos`
--

CREATE TABLE `projetos` (
  `idProjeto` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `descricao` text NOT NULL,
  `participantes` int(11) DEFAULT NULL,
  `inicio` date DEFAULT NULL,
  `termino` date DEFAULT NULL,
  `criador` varchar(50) NOT NULL,
  `finalizado` tinyint(1) NOT NULL DEFAULT '0',
  `idExterno` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `projetos`
--

INSERT INTO `projetos` (`idProjeto`, `nome`, `descricao`, `participantes`, `inicio`, `termino`, `criador`, `finalizado`, `idExterno`) VALUES
(36, 'Projeto02', 'funcionando normalmente, cadastrado a partir da plataforma', NULL, '2018-04-05', '2018-12-31', 'admin', 1, 0),
(38, 'Projeto03', 'Agora, finalmente, está funcionando.', NULL, '2018-04-06', '2018-12-31', 'admin', 0, 0),
(53, 'Projeto04', 'AGORA FUNCIONOU DA MANEIRA ESPERADA.', NULL, '2018-04-06', '2018-12-31', 'admin', 0, 0),
(55, 'Projeto05', 'vamos ver o que acontece quando a descricao do projeto for grande.vamos ver o que acontece quando a descricao do projeto for grande.vamos ver o que acontece quando a descricao do projeto for grande.vamos ver o que acontece quando a descricao do projeto for grande.vamos ver o que acontece quando a descricao do projeto for grande.vamos ver o que acontece quando a descricao do projeto for grande.vamos ver o que acontece quando a descricao do projeto for grande.vamos ver o que acontece quando a descricao do projeto for grande.vamos ver o que acontece quando a descricao do projeto for grande.vamos ver o que acontece quando a descricao do projeto for grande.', NULL, '2018-01-25', '2018-02-11', 'admin', 0, 0),
(56, 'Projeto06', 'Agora a ediçao funciona', NULL, '2018-04-12', '2018-04-27', 'admin', 0, 0),
(59, 'Projeto07', 'quase tudo funcionando.', NULL, '2018-04-12', '2018-12-31', 'admin', 0, 0),
(60, 'Projeto08', 'finalmente funcionando', NULL, '2018-04-12', '2018-12-31', 'admin', 0, 0),
(69594, 'Projeto01', 'Projeto editado na ferramenta, edicao funcionando completamente.', NULL, '2018-04-05', '2018-12-31', 'admin', 1, 16);

-- --------------------------------------------------------

--
-- Estrutura da tabela `respostas`
--

CREATE TABLE `respostas` (
  `idResposta` int(11) NOT NULL,
  `resposta` varchar(500) NOT NULL,
  `idComentario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `respostas`
--

INSERT INTO `respostas` (`idResposta`, `resposta`, `idComentario`) VALUES
(2, 'hguyfutftfyf', 22),
(3, 'ashiaushisuhs', 23);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipos`
--

CREATE TABLE `tipos` (
  `idTipo` int(11) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `rgba` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipos`
--

INSERT INTO `tipos` (`idTipo`, `tipo`, `rgba`) VALUES
(0, 'Dúvida', 'rgba(0,255,0,0.3)'),
(1, 'Silêncio', 'rgba(100,155,0,0.3)');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `nome` varchar(50) NOT NULL,
  `senha` varchar(10) NOT NULL,
  `tipo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`nome`, `senha`, `tipo`) VALUES
('admin', '1234', 'admin'),
('super', '1234', 'supervisor'),
('user', '123', 'usuario'),
('user2', '123', 'usuario');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audios`
--
ALTER TABLE `audios`
  ADD PRIMARY KEY (`idAudio`),
  ADD KEY `idProjeto` (`idProjeto`),
  ADD KEY `fk_nomeUsuario` (`usuario`);

--
-- Indexes for table `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`idComentario`),
  ADD KEY `idAudio` (`idAudio`),
  ADD KEY `fk_idTipo` (`idTipo`);

--
-- Indexes for table `projetos`
--
ALTER TABLE `projetos`
  ADD PRIMARY KEY (`idProjeto`),
  ADD UNIQUE KEY `nome` (`nome`),
  ADD KEY `criador` (`criador`);

--
-- Indexes for table `respostas`
--
ALTER TABLE `respostas`
  ADD PRIMARY KEY (`idResposta`),
  ADD KEY `fk_idComentario` (`idComentario`);

--
-- Indexes for table `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`idTipo`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`nome`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audios`
--
ALTER TABLE `audios`
  MODIFY `idAudio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `idComentario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `projetos`
--
ALTER TABLE `projetos`
  MODIFY `idProjeto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69595;
--
-- AUTO_INCREMENT for table `respostas`
--
ALTER TABLE `respostas`
  MODIFY `idResposta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tipos`
--
ALTER TABLE `tipos`
  MODIFY `idTipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `audios`
--
ALTER TABLE `audios`
  ADD CONSTRAINT `audios_ibfk_1` FOREIGN KEY (`idProjeto`) REFERENCES `projetos` (`idProjeto`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_nomeUsuario` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`nome`);

--
-- Limitadores para a tabela `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `comentarios_ibfk_1` FOREIGN KEY (`idAudio`) REFERENCES `audios` (`idAudio`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_idTipo` FOREIGN KEY (`idTipo`) REFERENCES `tipos` (`idTipo`);

--
-- Limitadores para a tabela `projetos`
--
ALTER TABLE `projetos`
  ADD CONSTRAINT `projetos_ibfk_1` FOREIGN KEY (`criador`) REFERENCES `usuarios` (`nome`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `respostas`
--
ALTER TABLE `respostas`
  ADD CONSTRAINT `fk_idComentario` FOREIGN KEY (`idComentario`) REFERENCES `comentarios` (`idComentario`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
