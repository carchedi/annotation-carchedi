<?php
    session_start();
    if ($_SESSION['NOME'] == null) {
        header('Location: index.html');
    }
    /*
    }elseif($_GET['id'] == null){
        if($_SESSION['TIPO'] == 'user'){
            header('Location: user_visaoGeral.php');
        }else{
            header('Location: admin_visaoGeral.php');
        }                
    }*/
?>

<html>
	<head>
	 	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" src="css/style.css"> 
        <link rel="stylesheet" href="css/app.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title>Ferramenta para Anotação</title>

        <!-- wavesurfer.js -->
        <script src="js/wavesurfer.min.js"></script>

        <!-- plugins -->
        <script src="js/wavesurfer.timeline.min.js"></script>
        <script src="js/wavesurfer.regions.min.js"></script>
        <script src="js/wavesurfer.minimap.min.js"></script>

        <!-- App-->
        <script src="js/trivia.js"></script>

        <!--Script para o Ajax-->
        <script src="js/ajax.js"></script>

        <script src="js/app.js"></script> 

        <style type="text/css">
            #ex1Slider .slider-selection {
                background: #BABABA;
            }
        </style>

        <script type="text/javascript">
            
            function navegar(endereço){
                window.location.href=endereço;
            }

            function submeter(estado, idAudio, usuario){ 
                var coments = regionsToDB();
                var qtdErro = document.getElementById('qtdErro').value | 0;
                var ultimaPalavra = document.getElementById('ultimaPalavra').value | 0;
                var pausaSentido = $('input[id=pausaSentido]:checked').val() | 0;
                var resultado = $('input[id=oralidade]:checked').val();
                if(document.getElementById('qtdErro').value !="" && document.getElementById('ultimaPalavra').value !=""){
                    if(resultado == undefined)
                        resultado = -1;
                    if(resultado == -1 && estado == 3){
                        alert('Dê um parecer sobre a oralidade antes de continuar!');
                    }else{
                        var destino = "controle_annotation_tool.php?idAudio="+idAudio+"&estado="+estado+"&qtdErro="+qtdErro+"&ultimaPalavra="+ultimaPalavra+"&pausaSentido="+pausaSentido+"&resultado="+resultado+"&newcom="+coments+"&usuario="+ usuario;
                        window.location.href = destino;
                    } 
                }else{
                    alert('Antes de continuar, informe a última palavra lida e a quatidade de palavras não lidas.');
                }  
            }

            function addComment(){      
                wavesurfer.addRegion({
                    id: 'sample',
                    start: wavesurfer.getCurrentTime()-2,         
                    end: wavesurfer.getCurrentTime(),            
                    color: 'rgba(50,112,255,0.5)'
                });  
            }

            
            function mudacor(){
			    wavesurfer.on('region-updated', function(region){
			       // region.update({color:"rgba(0,200,0,0.3)"});
			    });      	

            }

            function corretas(){
                var x = document.getElementById('ultimaPalavra').value;
                var y = document.getElementById('qtdErro').value;
                document.getElementById('palavrasCorretas').value = x-y;
            }
        </script>
        
	</head>
	<body>
    <?php
        include_once 'includes/functions.inc.php';
        include_once 'includes/db_connect.inc.php';
        ini_set("default_charset", "UTF-8");

        $audio = selecionaAudio($_SESSION['NOME'],$mysqli);

        echo '<script type="text/javascript">';
        if($audio){
            if(file_exists($audio['caminho'])){
                echo "$(document).ready(function() {
                var event = new CustomEvent('carregarAudio', { detail: '".$audio['caminho']."' });
                document.dispatchEvent(event);
                });";
                echo "$(document).ready(function() {
                var event = new CustomEvent('carregarComentarios', { detail: '".$audio['idAudio']."' });
                document.dispatchEvent(event);
                });";
            }else{
                echo 'alert("Ocorreu um problema ao carregar o áudio. O caminho para o arquivo é inválido:'.$audio['caminho'].'"); window.location.href = "user_visaoGeral.php";';
            }
        }else{
            echo 'alert("Não há áudios para serem processados");';            
            echo 'window.location.href = "user_visaoGeral.php";';
        }
        echo '</script>';

    ?>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <div class="navbar-brand">Ferramenta para Anotação</div>
            </div>
            <ul class="nav navbar-nav navbar-right"> 
                <li><a href="logout.php">Sair</a></li>
            </ul>
        </div>
    </nav> 

    <div>
        
        <div class="container text-center"
             style="background-color:#fff; box-shadow: 0 2px 4px rgba(0,0,0,.15);padding-bottom: 40px; margin-top: 40px; width:80%">
 
            <div class = "row">
                <div class="col-sm-12">
                    <h5 itemprop="name">
                        <?php
                            echo "Áudio: ".$audio['idAudio'];
                        ?>
                    </h5>                
                </div>
            </div> 
            <div class = "row">
                <div class = "col-sm-12">
                    <p id="subtitle" class="text-center text-info">&nbsp;</p>
                    <p id="subtitle2" class="text-center text-info">&nbsp;</p>
                    <div id="wave-timeline" style="text-align: left;"></div>
                    <div id="waveform">
                        <!-- Here be waveform -->
                    </div>
                </div>
            </div>
            <!--PLAY/PAUSE E COMENTAR-->
            <div id="demo">   
                <div class="row" style="margin: 10px 0">
                    <div class="col-sm-2">
                        <button class="btn btn-primary btn-block" data-action="play">
                            <span id="play">
                                <i class="glyphicon glyphicon-play"></i>
                                Play
                            </span>

                            <span id="pause" style="display: none">
                                <i class="glyphicon glyphicon-pause"></i>
                                Pause
                            </span>
                        </button>
                    </div>
                    <div class="col-sm-2">
                     <button class="btn btn-primary btn-block" onclick= addComment() title="Comment">
                        Comentar
                    </button>                     
                    </div>
                </div>
            </div>

            <div class = "row">
                <!-- Carrega texto do servidor -->
                <div class="col-sm-6" style="margin: 0px 0; border: 0px; text-align: justify; text-justify: inter-word; overflow: hidden; overflow-y: scroll; max-height: 40%;">
                	<?php                    
                        $url = 'http://200.131.219.35/oralidade/index.php/texts/'.$audio['idExterno'];
                		$ch = curl_init($url);
    					curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
    					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    					$data = curl_exec($ch);
    					$http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    					if(curl_errno($ch) == 0 AND $http == 200) {
    						$text = file_get_contents($url);  
    	            		$textoComIndice = json_decode($text) -> text_content;
                            $textoComIndice = explode(' ', $textoComIndice);
                            foreach($textoComIndice as $index => $palavra){
                                echo $palavra.'<font color="red"><sup>'.($index+1).'</sup></font> ';
                            }
    	            	}else{
                			echo "O texto não foi encontrado no servidor.";
                   		}                    
                	?>
                </div>   
                <div class="col-sm-6">  
                    <form role="form" name="edit" style="opacity: 0; transition: opacity 300ms linear; margin: 3px 0;">                        
                        <div class="col-sm-12" style="margin: 0px 0; border: 0px">
                            <div class="row" style="margin: 0px 0; border: 0px; text-align: left;"> 
                                <input class="form-control" type="hidden" id="start" name="start" style="width: 1px"/>  
                                <input class="form-control" type="hidden" id="end" name="end" style="width: 1px" />                           
                                <?php
                                    $tipos = recuperaTipos($mysqli);
                                    foreach($tipos as $tipo){
                                        echo'<label>
                                                <input type="radio" id = "'.$tipo['idTipo'].'" name="optradio" value = "'.$tipo['rgba'].'">'.$tipo['tipo'].'
                                            </label>&nbsp&nbsp&nbsp&nbsp';
                                    }
                                ?>
                                <textarea id="note" class="form-control" name="note" rows="4" cols="50"></textarea>
                            </div>

                            <div class="row" style="margin-top:5px; margin-bottom: 30px; margin-left: 0px 0"> 
                                <div class="col-sm-3"> 
                                </div>
                                <div class="col-sm-3"> 
                                </div>
                                <div class="col-sm-3">
                                    <button type="submit" onclick="mudacor()" class="btn btn-success btn-block">Salvar</button>
                                </div>
                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-danger btn-block" data-action="delete-region">Apagar</button>
                                </div>   
                            </div>  
                        </div>
                    </form>               
                    <table style="width: 80%">
                        <tr><td>Número da última palavra lida:</td><td>                         
                            <input id="ultimaPalavra" onchange=corretas(); type="number" min="0" max=<?php if(isset($index)) echo "\"".($index+1)."\"" ?> >
                        </td></tr>
                        <tr><td>Quantidade de palavras não lidas ou com erro:</td><td>
                            <input id="qtdErro" onchange=corretas(); type="number" min="0" max=<?php if(isset($index)) echo "\"".($index+1)."\"" ?> >
                        </td></tr> 
                        <tr><td>Palavras lidas corretamente:</td><td>                         
                            <input id="palavrasCorretas" type="number" min="1" max=<?php if(isset($index)) echo "\"".($index+1)."\"" ?> disabled>
                        </td></tr> 

                        <tr><td>Obedece as pausas de sentido?</td>
        					<td>
        					  <input id = "pausaSentido" type="radio" name="sentido" value="1">Sim
        					  <input id = "pausaSentido" type="radio" name="sentido" value="0">Não  
        					</td></tr>
                        <tr><td>Parecer final sobre a oralidade:</td>
                            <td>
                              <input id = "oralidade" type="radio" name="oralidade" value="1">Boa
                              <input id = "oralidade" type="radio" name="oralidade" value="0">Ruim  
                            </td></tr>               
                    </table> 
                </div>
            </div>
            <!--CONCLUIDO E EXPORT -->
            <div class="container">            
                <div id="demo">   
                    <div class="row" style="margin: 10px 0">
                        <div class="col-sm-2"> 
                            <?php
                            echo "<button class='btn btn-primary btn-block' id='concluido'
                                              onclick=submeter(3,".$audio['idAudio'].",\"".$_SESSION['NOME']."\")>
                                <span>
                                    Avaliação Terminada
                                </span>
                            </button>";
                            ?>
                        </div>
                        <div class="col-sm-2"> 
                            <?php
                            echo "<button class='btn btn-primary btn-block' id='concluido'
                                              onclick=submeter(2,".$audio['idAudio'].",\"".$_SESSION['NOME']."\")>
                                <span>
                                    Existem Dúvidas
                                </span>
                            </button> ";
                            ?>
                        </div> 
                    </div>
                </div>
            </div>

        <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', 'UA-50026819-1', 'wavesurfer.fm');
                ga('send', 'pageview');  
        </script> 
    </div>
	</body>
</html>
