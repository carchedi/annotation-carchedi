

<html>
	<head>
	 	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        <title> Medição de Oralidade </title>
	</head>
	<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/index.php">Medição de Oralidade</a>
            </div>

            <ul class="nav navbar-nav">
                <li class="active"><a href="/sistema/index.php">Home</a></li>
                <li><a href="/sistema/cadastra_texto.php">Cadastrar Textos</a></li>
                <li><a href="/sistema/texto.php">Gerenciar Textos</a></li>
                <li><a href="/sistema/cria_dicionario.php">Gerenciar Dicionários</a></li>
                <li><a href="/sistema/cria_modelo.php">Gerenciar Modelos</a></li>
                <li><a href="/sistema/audio.php">Serviços</a></li>


            </ul>
        </div>

    </nav>
    <h2 class="text-center">Projeto de Medição de Oralidade </h2>
    <div class="h-100 d-flex container">
        <div class="jumbotron my-auto">
            <p class="text-center">Quem somos nós?</p>
            <hr>
            Este projeto está sendo desenvolvido pelo Centro de Políticas Públicas e Avaliação da Educação (CAEd/UFJF)
            com financiamento da Fundação de Apoio e Desenvolvimento ao Ensino, Pesquisa e Extensão (Fadepe). O projeto
            tem o objetivo de extrair automaticamente parâmetros de áudios referentes à leituras de crianças
            em fase de alfabetização que permitam aos profissionais de educação realizarem a avaliação da leitura
            dos alunos de forma mais raṕida.
            <hr>
            <p class="text-center">Funcionamento</p>
            <hr>
        </div>
	</body>
</html>
