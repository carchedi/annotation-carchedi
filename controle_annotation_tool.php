<?php
	include_once 'includes/functions.inc.php';
	include_once 'includes/db_connect.inc.php';

	if(isset($_GET['idAudio']) && isset($_GET['estado']) && isset($_GET['qtdErro']) && isset($_GET['ultimaPalavra']) && isset($_GET['pausaSentido']) && isset($_GET['newcom']) && isset($_GET['resultado']) && isset($_GET['usuario'])){
		if(editaAudio($_GET['idAudio'],$_GET['estado'],$_GET['qtdErro'],$_GET['pausaSentido'],$_GET['ultimaPalavra'], $_GET['resultado'], $_GET['usuario'], $mysqli)){
			echo 'Audio editado com sucesso';
			$comentarios = json_decode($_GET['newcom'], true);
			foreach ($comentarios as $comentario) {
				if(substr($comentario['id'], 0,4) == 'wave' || substr($comentario['id'], 0,6) == 'sample'){
					criaComentario($comentario['data']['note'], $comentario['data']['type'], $comentario['start'], $comentario['end'], $comentario['color'],$_GET['idAudio'], $mysqli);
				}else{
					editaComentario($comentario['id'], $comentario['data']['note'], $comentario['data']['type'], $comentario['start'], $comentario['end'], $comentario['color'], $mysqli);
				}
			}
		}else{
			echo 'Audio não editado com sucesso';
		}
	}else{
		echo 'Parâmetros não passados corretamente';
	}
	header('Location: user_visaoGeral.php');

?>