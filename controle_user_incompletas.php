<?php
	include_once 'includes/functions.inc.php';
	include_once 'includes/db_connect.inc.php';

	if(isset($_GET['idAudio']) && isset($_GET['estado']) && isset($_GET['resultado'])){
		if(finalizaDuvidaAudio($_GET['idAudio'],$_GET['estado'],$_GET['resultado'], $mysqli)){
			echo 'Audio finalizado com sucesso';
		}else{
			echo 'Audio não finalizado com sucesso';
		}
	}else{
		echo 'Parâmetros não passados corretamente';
	}
	header('Location: user_visaoGeral.php');
?>