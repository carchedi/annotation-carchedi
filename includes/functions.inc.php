<?php
 
function verificaValidadeCampo($campo){ //testa se um campo está preenchido - ja faço via javascript
    if(isset($campo) && $campo != ""){
        return true;    
    }
    return false; 
}

/*Verifica a Existência do Usuário*/
function verificaUsuario($nome_form, $senha_form, $mysqli){
    if ($stmt = $mysqli->prepare("SELECT nome, senha, tipo        
                                  FROM usuarios
                                  WHERE nome = ? LIMIT 1")) { //verifica se a query é válida

        // Atribui "$id_pessoa" ao parâmetro. 
        $stmt->bind_param('s', $nome_form);    

        $stmt->execute();   // Executa a query.
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            // Caso o usuário exista, pega variáveis a partir do resultado.                 
            $stmt->bind_result($nomeVindoBanco, $senhaVindaBanco, $tipoDB);
            $stmt->fetch();
            return array('nome'=>$nomeVindoBanco, 'senha'=>$senhaVindaBanco, 'tipo'=>$tipoDB);
        }
        return null;
    }  
}

/*Cria um novo projeto*/
function criaProjeto($nome, $descricao, $inicio, $termino, $criador, $idExterno, $mysqli){
    if ($stmt = $mysqli->prepare("INSERT INTO projetos(nome,descricao,inicio,termino,idExterno,criador) 
                                  VALUES ('$nome','$descricao','$inicio','$termino',$idExterno,'$criador');")) { //verifica se a query é válida
        echo "Query valida!";
        $stmt->execute();  
        return;
    } 
}

/*Finalizar um projeto*/
function finalizaProjeto($nome, $mysqli){
    if ($stmt = $mysqli->prepare("UPDATE `projetos` SET `finalizado`= 1 WHERE `nome`='$nome'")) { //verifica se a query é válida
        $stmt->execute(); 
        return;
    }   
}

/*Editar os dados de um projeto*/
function editaProjeto($idProjeto, $nome, $descricao, $inicio, $termino, $mysqli){  
            //verificar se o usuario é o dono do projeto - nao sei se isso vai ser implementado.
    if ($stmt = $mysqli->prepare("UPDATE `projetos` SET `nome`='$nome',`descricao`='$descricao',`inicio`='$inicio',`termino`='$termino' WHERE `idProjeto`='$idProjeto'")) { //faz o update se a query for válida
        $stmt->execute();   
        return;
    } 
}

function recuperaProjetos($mysqli){
    $query = "SELECT * FROM projetos";
    if($stmt = $mysqli->prepare($query)){
        //echo "Query válida!";
        $result = array();
        $data = $mysqli->query($query);
        while($row = $data->fetch_assoc()){
            array_push($result, $row);
        }
        return $result;
    }else{
        //echo "Query não valida";
        return null;
    }
}

function recuperaComentarios($idAudio, $mysqli){
    $query = "SELECT * FROM comentarios WHERE idAudio = $idAudio AND idTipo = 0";
    if($stmt = $mysqli->prepare($query)){
        //echo "Query válida!";
        $result = array();
        $data = $mysqli->query($query);
        while($row = $data->fetch_assoc()){
            array_push($result, $row);
        }
        return $result;
    }else{
        //echo "Query não valida";
        return null;
    }
}

function recuperaRespostas($idComentario, $mysqli){
    $query = "SELECT respostas.*, comentarios.idAudio FROM respostas INNER JOIN comentarios ON respostas.idComentario = comentarios.idComentario WHERE respostas.idComentario = $idComentario;";
    if($stmt = $mysqli->prepare($query)){
        //echo "Query válida!";
        $result = array();
        $data = $mysqli->query($query);
        while($row = $data->fetch_assoc()){
            array_push($result, $row);
        }
        return $result;
    }else{
        //echo "Query não valida";
        return null;
    }
}

/*Funcao pra saber quais audios tem duvidas em aberto*/
function recupera_duvidas($usuario, $param ,$mysqli){ 
    $query = "SELECT DISTINCT comentarios.idAudio, audios.caminho FROM comentarios INNER JOIN audios ON audios.idAudio = comentarios.idAudio WHERE audios.estado = 2 ";

    if ($param == 0) { //duvidas DESTE usuario
        $query = $query."AND audios.usuario = '$usuario'";
    }else{ //duvidas de OUTROS usuarios
        $query = $query."AND audios.usuario <> '$usuario'";
    } 
    
    if($stmt = $mysqli->prepare($query)){  //Query válida!
        $result = array();
        $data = $mysqli->query($query);
        while($row = $data->fetch_assoc()){
            array_push($result, $row);
        } 
        return $result;
    }else{
        //echo "Query não valida";
        return null;
    }

}

function criaComentario($comentario, $idTipo, $inicio, $fim, $cor, $idAudio, $mysqli){
    $query = "INSERT INTO comentarios(comentario, idTipo, inicio, fim, cor, idAudio) 
                                  VALUES ('$comentario', $idTipo, $inicio, $fim,'$cor',$idAudio);";
    if ($stmt = $mysqli->prepare($query)) {
        $stmt->execute();  
        return;
    }
}

function criaResposta($resposta, $idComentario, $mysqli){
    echo 'Conferindo resposta... '.$resposta;
    $query = "INSERT INTO respostas(resposta, idComentario) VALUES ('$resposta', $idComentario);";
    if ($stmt = $mysqli->prepare($query)) {
        if($stmt->execute()) 
            return true;
    }
    echo $query;
    return false;
}

function editaComentario($idComentario, $comentario, $idTipo, $inicio, $fim, $cor, $mysqli){
    if ($stmt = $mysqli->prepare("UPDATE comentarios SET comentario = '$comentario', idTipo = $idTipo,
        inicio = $inicio, fim = $fim, cor = '$cor' WHERE idComentario = $idComentario")) { 
        if($stmt->execute()){
            echo 'Sucesso';
        }else{
            echo 'Fracasso';
        }   
        return;
    }
}

function criaAudio($caminho, $estado, $resultado, $idProjeto, $mysqli){
    $query = "SELECT idProjeto FROM projetos WHERE idExterno = $idProjeto";
    if($stmt = $mysqli->prepare($query)){
        $handler = $mysqli->query($query);        
        if($idBase = $handler->fetch_assoc()['idProjeto']){
            $query = "INSERT INTO audios(caminho,estado,resultado,idProjeto) VALUES ('$caminho','$estado','$resultado','$idBase');";
            if ($stmt = $mysqli->prepare($query)) { //verifica se a query é válida
                echo "Query valida!";
                if($stmt->execute()){
                    return true;
                }
            } 
        }
    }
    return false;
}

function editaAudio($idAudio, $estado, $qtdErro, $pausaSentido, $ultimaPalavra, $resultado, $usuario, $mysqli){
    $query = "UPDATE audios SET estado = $estado, qtdErro = $qtdErro,
        pausaSentido = $pausaSentido, ultimaPalavra = $ultimaPalavra, resultado = $resultado, usuario = '$usuario' WHERE idAudio = $idAudio";
    if ($stmt = $mysqli->prepare($query)) { 
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
    echo 'Query: '.$query;
    return false;
}

function finalizaDuvidaAudio($idAudio, $estado, $resultado, $mysqli){
    $query = "UPDATE audios SET estado = $estado, resultado = $resultado WHERE idAudio = $idAudio";
    if ($stmt = $mysqli->prepare($query)) { 
        if($stmt->execute()){
            return true;
        }
    }
    echo 'Query: '.$query;
    return false;
}


/*
    Seleciona áudios da base que devem ser avaliados, ou seja, que possuem estado = 0
*/
function selecionaAudio($usuario, $mysqli){
    $query = "SELECT audios.*, projetos.idExterno FROM audios INNER JOIN projetos ON audios.idProjeto = projetos.idProjeto WHERE estado = 0 AND usuario = '$usuario' ORDER BY RAND() LIMIT 1";
    $result = array();
    if($stmt = $mysqli->prepare($query)){
        //echo 'Query válida!';
        $data = $mysqli->query($query);
        $row = $data->fetch_assoc();
        $query = "UPDATE audios SET audios.estado = 1 WHERE audios.idAudio = ".$row['idAudio'];
        if($stmt = $mysqli->prepare($query))
            $stmt->execute();
        return $row;
    }
    return null;
}

function recuperaTipos($mysqli){
    $query = "SELECT * FROM tipos";
    if($stmt = $mysqli->prepare($query)){
        //echo "Query válida!";
        $result = array();
        $data = $mysqli->query($query);
        while($row = $data->fetch_assoc()){
            array_push($result, $row);
        }
        return $result;
    }else{
        //echo "Query não valida";
        return null;
    }    
}