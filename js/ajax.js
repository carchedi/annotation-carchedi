var  regionsDB;
document.addEventListener('carregarComentarios', function (data) {
	httpRequest = new XMLHttpRequest();
	if (!httpRequest) {
		alert('Giving up :( Cannot create an XMLHTTP instance');
	}
	httpRequest.onreadystatechange = alertContents;
	httpRequest.open('GET', 'comentarios.php?id=' + data.detail);
	httpRequest.send();

	function alertContents() { 
		if (httpRequest.readyState === XMLHttpRequest.DONE) {
			if (httpRequest.status === 200) { 
				console.log(httpRequest.responseText);
				regionsDB = JSON.parse(httpRequest.responseText);
			} else {
				alert('There was a problem with the request.');
			}
		}
	}
});