/**
 * Create a WaveSurfer instance.
 /**
    cores RGBA
        vermelho (255,0,0,0.4)
        azul     (0,0,200,0.4)
        verde    (0,150,0,0.4)
        amarelo  (255,255,0,0.4)
        laranja  (255,112,10,0.5)
 **/
var regiaoativa = null;
var wavesurfer = Object.create(WaveSurfer);

/**
 * Init & load.
 */
document.addEventListener('carregarAudio', function (data) {
    // Init wavesurfer
    console.log(data);
    wavesurfer.init({
        container: '#waveform', 
        waveColor:  'rgba(128,128,128,1)', //COR DA ONDA
        progressColor: 'rgba(28, 107, 243, 1)', //COR DA PARTE QUE JÁ PASSOU
        height: 100,
        pixelRatio: 1,
        scrollParent: true,
        normalize: true,
        minimap: true,
        backend: 'MediaElement'
    });

    wavesurfer.load(data.detail);

    wavesurfer.on('error', function(error){
        alert("Ocorreu um erro com o áudio: " + error);
    })

    /* Regions */  //SELECIONAR ARRASTANDO O MOUSE
    wavesurfer.enableDragSelection({     
    	color: randomColor(0.1) //alterei a funcao pra sempre fazer da mesma cor.
    });
    

    wavesurfer.on('ready', function () {        
        loadRegions();
    });

    wavesurfer.on('region-click', function (region, e) {
        e.stopPropagation();
        // Play on click, loop on shift click
        e.shiftKey ? region.playLoop() : region.play();
    });
    wavesurfer.on('region-click', editAnnotation);
    wavesurfer.on('region-updated', saveRegions);
    wavesurfer.on('region-removed', saveRegions);
    wavesurfer.on('region-in', showNote);

    wavesurfer.on('region-play', function (region) {
        region.once('out', function () {
            wavesurfer.play(region.start);
            wavesurfer.pause();
        });
    });

    /* Minimap plugin */   //ONDA DE BAIXO.
    wavesurfer.initMinimap({
        height:30,
        waveColor:'#ddd',
        progressColor:'rgba(0,ff,0,1)',
        cursorColor:'#999'
    });


    /* Timeline plugin */
    wavesurfer.on('ready', function () {
        var timeline = Object.create(WaveSurfer.Timeline);
        timeline.init({
            wavesurfer: wavesurfer,
            container: "#wave-timeline"
        });
    });

    /* Toggle play/pause buttons. */
    var playButton = document.querySelector('#play');
    var pauseButton = document.querySelector('#pause');
    var commentButton = document.querySelector('#comment');

    wavesurfer.on('play', function () {
        playButton.style.display = 'none';
        pauseButton.style.display = '';
    });
    wavesurfer.on('pause', function () {
        playButton.style.display = '';
        pauseButton.style.display = 'none';
    }); 

}); 

/**
 * MANDAR AS REGIOES PARA O BANCO DE DADOS
 */
function regionsToDB(){ 
    return JSON.stringify(
        Object.keys(wavesurfer.regions.list).map(function (id) {
            var region = wavesurfer.regions.list[id];
            return {
                id: region.id,
                start: region.start,
                end: region.end,
                color: region.color,
                data: region.data
            };
        })
    ); 
}


/**
 * ORIGINAL salvando as anotações na variável regions
 */
function saveRegions() {
    regions = JSON.stringify(
        Object.keys(wavesurfer.regions.list).map(function (id) {
            var region = wavesurfer.regions.list[id];
            return {
                start: region.start,
                end: region.end,
                attributes: region.attributes,
                data: region.data
                //preciso colocar aqui a cor do comentário
            };
        })
    );    
}


/**
 * ORIGINAL carrega as regiões de regions
 */
function loadRegions() {
 regionsDB.forEach(
        function (region) { 
            //region.color = "rgba"+region.rgba;
            wavesurfer.addRegion(region);
        }
    );
}


/**
 * Extract regions separated by silence.
 */
function extractRegions(peaks, duration) {
    // Silence params
    var minValue = 0.0015;
    var minSeconds = 0.25;

    var length = peaks.length;
    var coef = duration / length;
    var minLen = minSeconds / coef;

    // Gather silence indeces
    var silences = [];
    Array.prototype.forEach.call(peaks, function (val, index) {
        if (val < minValue) {
            silences.push(index);
        }
    });

    // Cluster silence values
    var clusters = [];
    silences.forEach(function (val, index) {
        if (clusters.length && val == silences[index - 1] + 1) {
            clusters[clusters.length - 1].push(val);
        } else {
            clusters.push([ val ]);
        }
    });

    // Filter silence clusters by minimum length
    var fClusters = clusters.filter(function (cluster) {
        return cluster.length >= minLen;
    });

    // Create regions on the edges of silences
    var regions = fClusters.map(function (cluster, index) {
        var next = fClusters[index + 1];
        return {
            start: cluster[cluster.length - 1],
            end: (next ? next[0] : length - 1)
        };
    });

    // Add an initial region if the audio doesn't start with silence
    var firstCluster = fClusters[0];
    if (firstCluster && firstCluster[0] != 0) {
        regions.unshift({
            start: 0,
            end: firstCluster[firstCluster.length - 1]
        });
    }

    // Filter regions by minimum length
    var fRegions = regions.filter(function (reg) {
        return reg.end - reg.start >= minLen;
    });

    // Return time-based regions
    return fRegions.map(function (reg) {
        return {
            start: Math.round(reg.start * coef * 10) / 10,
            end: Math.round(reg.end * coef * 10) / 10
        };
    });
}

/**
 * Random RGBA color.
 */
function randomColor(alpha) {
    /*return 'rgba(' + [
        ~~(Math.random() * 255),
        ~~(Math.random() * 255),
        ~~(Math.random() * 255),
        alpha || 1
    ] + ')';*/
    return 'rgba(26, 129, 255, 0.6)';//colorindo de AZUL o comentário recém criado
}

/**
 * Edit annotation for a region.
 */
function editAnnotation (region) {
    var form = document.forms.edit;
    form.style.opacity = 1;
    //form.style.display = "block";    
    regiaoativa = region;
    form.elements.start.value = Math.round(region.start * 10) / 10,
    form.elements.end.value = Math.round(region.end * 10) / 10;
    form.elements.note.value = region.data.note || '';
    //form.elements.type.name = region.data.type || '';
    for (var i = 0; i < form.optradio.length; i++) {
        form.optradio[i].checked = false;
        if(form.optradio[i].id == region.data.type){
            form.optradio[i].checked = true;
        }        
    }
    form.onsubmit = function (e) {
        e.preventDefault(); 
        console.log("AQUI ->",form.elements);
        var newType = -1;
        for (var i = 0; i < form.optradio.length; i++) {
            if(form.optradio[i].checked){
                newType = form.optradio[i].id;
            }        
        }
        region.update({
            color: form.elements.optradio.value,
            start: form.elements.start.value,
            end: form.elements.end.value,
            data: {
                note: form.elements.note.value,
                type: newType
            }
        });
 
        form.style.opacity = 0; 
        //form.style.display = "none"; 
    };
    form.onreset = function () {
        form.style.opacity = 0;  
        form.dataset.region = null;
    };
    form.dataset.region = region.id;
}

/**
 * Display annotation.
 */
function showNote (region) {
    if (!showNote.el) {
        showNote.el = document.querySelector('#subtitle');
    }
    showNote.el.textContent = region.data.note || '- - - -';
}


/**
 * Bind controls.
 */
GLOBAL_ACTIONS['delete-region'] = function () {
    var form = document.forms.edit;
    var regionId = form.dataset.region;
    if (regionId) {
        wavesurfer.regions.list[regionId].remove();
        form.reset();
    }
};

GLOBAL_ACTIONS['export'] = function () {
    window.open('data:application/json;charset=utf-8,' +
        encodeURIComponent(regions));
};