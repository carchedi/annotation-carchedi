function change(){ 
	//funcao para os dados dos projetos sem recarregar a pagina.
    document.getElementById("form1").submit();
}


function enableButtons(param){ 	
	document.getElementById("novoProjeto").disabled = param;	
	document.getElementById("estatisticas").disabled = param;
	document.getElementById("Comentarios").disabled = param;
	document.getElementById("selectBox").disabled = param;
	document.getElementById("Editar").disabled = param;
	document.getElementById("Terminar").disabled = param;
	document.getElementById("Subir").disabled = param; 	 
	document.getElementById("Adicionar").disabled = param;
	document.getElementById("Remover").disabled = param;
} 


function novoProjeto(showhide){ 						
	//funcao para exibir/esconder o formulario de criacao de um projeto  
	if(showhide == 'show'){
		document.getElementById('labelProjeto').InnerHTML = "Criar um novo Projeto"; 
		document.getElementById('confirma').value = "Criar"; 
	    document.getElementById('popupbox').style.visibility="visible";	 	
	    document.getElementById('divtabela').style.visibility="visible";   
	    document.getElementById('operacaoH').value = 1; 
		enableButtons("true");	  
	}
	if(showhide == 'hide'){
		document.getElementById('nome').value='';
		document.getElementById('desc').value=''; 
		document.getElementById('popupbox').style.visibility="hidden";	 
		document.getElementById('divtabela').style.visibility="hidden"; 
		enableButtons(false);  
	}		
}   


function gerenciarUsuarios(showhide){
	//funcao para exibir/esconder o formulario de gerenciamento de usuarios  
	if(showhide == 'show'){
		document.getElementById('users').style.visibility="visible";  	
	    document.getElementById('divtabela').style.visibility="visible";  
		document.getElementById('operacaoH').value = 3;      
		enableButtons(true);
	}
	if(showhide == 'hide'){
		document.getElementById('users').style.visibility="hidden";    	 
		document.getElementById('divtabela').style.visibility="hidden";  
		document.getElementById('operacaoH').value =''; 	 
		enableButtons(false);
	}	
}

function gerenciarComentarios(showhide){
	//funcao para exibir/esconder o formulario de gerenciamento de usuarios  
	if(showhide == 'show'){
		document.getElementById('comments').style.visibility="visible";  	
	    document.getElementById('divtabela').style.visibility="visible";       
		enableButtons(true);
	}
	if(showhide == 'hide'){
		document.getElementById('comments').style.visibility="hidden";    	 
		document.getElementById('divtabela').style.visibility="hidden";  	 
		enableButtons(false);
	}	
}

function editarProjeto(showhide){ 						
	//funcao para exibir/esconder o formulario de edicao de um projeto  
	if(showhide == 'show'){	
		document.getElementById('idProjetoH').value = proj_id;
		document.getElementById('criadorH').value = proj_criador; 
		document.getElementById('nomeEDT').value = proj_nome;
		document.getElementById('descEDT').value = proj_desc;
		document.getElementById('periodoEDT').value = organizaDatas(proj_inicio+" - "+proje_termino); 			
	    document.getElementById('editarProjeto').style.visibility="visible";	 		    		 	 
		document.getElementById('divtabela').style.visibility="visible"; 
		document.getElementById('operacaoH').value = 3;
		enableButtons(true);	  
	}
	if(showhide == 'hide'){   
		document.getElementById('nome').value='';
		document.getElementById('desc').value=''; 
		document.getElementById('editarProjeto').style.visibility="hidden"; 
		document.getElementById('popupbox').style.visibility="hidden"; 		 
		document.getElementById('divtabela').style.visibility="hidden"; 
		//limpar o hidden aqui
		document.getElementById('operacaoH').value ='';
		enableButtons(false);   
	}			
}   

function submitForm(action){
	//funcao para criar um novo projeto a partir do formulario 
	if (action == 'criar') {  
		document.getElementById('nomeH').value = document.getElementById('nome').value; 
		document.getElementById('descricaoH').value = document.getElementById('desc').value; 
		document.getElementById('periodoH').value = document.getElementById('periodo').value;
		document.getElementById('criadorH').value = document.getElementById('criador').value; 
		document.getElementById('operacaoH').value = 1;  
		document.getElementById('escondido').submit(); //form 'hidden' com as informacoes preenchidas.
		novoProjeto('hide');
	}

	if (action == 'cancelar') {  
		editarProjeto('hide');  
		novoProjeto('hide');
	 }

	//funcao para finalizar o projeto selecionado
	if (action == 'finalizar') {
		if(confirm("finalizar o projeto \""+document.getElementById('selectBox').value+"\"?")){
			alert("o projeto \""+document.getElementById('selectBox').value+"\" foi finalizado.");
			document.getElementById('nomeH').value = document.getElementById('selectBox').value;
			document.getElementById('operacaoH').value = 2; 
			document.getElementById('escondido').submit();
		}
	}

	//funcao para editar o projeto selecionado
	if (action == 'editar') {  
			document.getElementById('nomeH').value = document.getElementById('nomeEDT').value; 
			document.getElementById('operacaoH').value = 3; 
 			document.getElementById('descricaoH').value = document.getElementById('descEDT').value;
 			document.getElementById('periodoH').value =document.getElementById('periodoEDT').value;    
			document.getElementById('escondido').submit();
			editarProjeto('hide');
	}
}

function organizaDatas(datas){
	str = datas.replace(" - ", "-");
	str = str.split("-");
	return str[2]+"/"+str[1]+"/"+str[0]+" - "+str[5]+"/"+str[4]+"/"+str[3]
}