<?php
include_once 'includes/db_connect.inc.php';
include_once 'includes/functions.inc.php';
$nome = $_POST['nome'];
$senha = $_POST['senha']; 
 
    if ($nome != null) { //se existem valores para usuario e senha
	    session_start();
	    $_SESSION['NOME'] = $nome;
	    $_SESSION['SENHA'] = $senha;  

        $user = verificaUsuario($nome, $senha, $mysqli); 
        if ($user != null) {  //se o usuario informado existe no banco de dados
            if ($senha == $user['senha']) {  
                $_SESSION['TIPO'] = $user['tipo'];
                if ($user['tipo'] == 'usuario') {
                   header('Location: user_visaoGeral.php');
                }else if($user['tipo'] == 'admin'){
                   header('Location: admin_visaoGeral.php');
                }else{
                   header('Location: super_visaoGeral.php');
                }
            }else {            	
            	header('Location: index2.php');
            }
        }else{         		// o usuario informado NAO EXISTE no banco de dados
            header('Location: index2.php');
        } 
    }else{ 
    	header('Location: index.html');
    }
?>