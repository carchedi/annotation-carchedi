<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Ferramenta Anotação</title> 

        <!-- Bootstrap -->
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
 
        <link rel="css/stylesheet" href="css/ribbon.css" />
        <link rel="css/stylesheet" href="css/app.css" />
        <link rel="stylesheet" type="text/css" href="css/table.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="css/screenshot" itemprop="screenshot" href="https://katspaugh.github.io/wavesurfer.js/example/screenshot.png" />

        <!-- wavesurfer.js -->
        <script src="js/wavesurfer.min.js"></script>

        <!-- plugins -->
        <script src="js/wavesurfer.timeline.min.js"></script>
        <script src="js/wavesurfer.regions.min.js"></script>
        <script src="js/wavesurfer.minimap.min.js"></script>

        <!-- App-->
        <script src="js/trivia.js"></script>

        <!--Script para o Ajax-->
		<script src="js/ajax.js"></script>

        <script src="js/app.js"></script> 

        <script type="text/javascript">
        	function navegar(endereço){
        		window.location.href=endereço;
        	}

        	
			function addComment(){      
                wavesurfer.addRegion({
                    id: 'sample',
                    start: wavesurfer.getCurrentTime()-2,         
                    end: wavesurfer.getCurrentTime(),            
                    color: 'rgba(50,112,255,0.5)'
                });  
			}
        </script>
     </head>

    <body itemscope itemtype="http://schema.org/WebApplication">
        <?php 
            session_start(); 
			if ($_SESSION['NOME'] == null) {
	            header('Location: index.html');
	        }elseif($_GET['id'] == null){
                if($_SESSION['TIPO'] == 'user'){
                    header('Location: principal_user.php');
                }else{
                    header('Location: principal_admin.php');
                }                
            }
            
        ?>
        <div class="container text-center"
             style="background-color:#fff; box-shadow: 0 2px 4px rgba(0,0,0,.15);padding-bottom: 40px; margin-top: 40px; width:80%">

            <div class="container"> 
                <h3 itemprop="name">Ferramenta para anotação</h3>
            </div>

            <div class="container">
                <div class="col-sm-4"></div>
                <div class="col-sm-4">
                    <h5 itemprop="name">
                        <?php
                            echo "Áudio_".$_GET['id'];
                        ?>
                    </h5>                
                </div>
                <div class="col-sm-4" style="text-align: right;">
                    <a href="logout.php">Sair</a>
                </div>
            </div>

            <div class="container">
                <p id="subtitle" class="text-center text-info">&nbsp;</p> 
                <div id="wave-timeline" style="text-align: left;"></div>
                <div id="waveform">
                    <!-- Here be waveform -->
                </div>
            </div>

            <div class="container">
                <div id="demo">   
                    <div class="row" style="margin: 10px 0">
                        <div class="col-sm-2">
                            <button class="btn btn-primary btn-block" data-action="play">
                                <span id="play">
                                    <i class="glyphicon glyphicon-play"></i>
                                    Play
                                </span>

                                <span id="pause" style="display: none">
                                    <i class="glyphicon glyphicon-pause"></i>
                                    Pause
                                </span>
                            </button>
                        </div>
                        <div class="col-sm-2">
                         <button class="btn btn-primary btn-block" onclick= addComment() title="Comment">
                            Comentar
                        </button>

                     
                        </div>
                    </div>
                </div>
            </div>

     
            <div class="container">        
                <div class="col-sm-12" style="margin: 0px; text-align: left;"> 
                    <form role="form" name="edit" style="opacity: 0; transition: opacity 300ms linear; margin: 3px 0;">
                        <div class="col-sm-6" style="margin: 0px 0; border: 0px">
                            <div class="row" style="margin: 0px 0; border: 0px">
                                <div class="col-sm-2"  style="margin: 0px 0; border: 0px">
                                   <label for="start">Início</label>
                                   <input class="form-control" id="start" name="start" style="width: 80px" />
                                </div>
                                <div class="col-sm-2"  style="margin: 0px 0; border: 0px">
                                   <label for="end">Fim</label>
                                   <input class="form-control" id="end" name="end" style="width: 80px"/>
                                </div>
                            </div>

                            <div class="col-sm-12" style="margin: 10px 0; border: 0px">
                                <label for="note">Comentário</label>
                                <textarea id="note" class="form-control" name="note" rows="4" cols="50"></textarea>
                            </div>

                            <div class="col-sm-12" style="margin-top: 0px 0">
                                <label>Tipo</label><br>
                                <div class="col-sm-3" style="margin-top: 0px 0"> 
                                    <div class="radio">
                                      <label><input type="radio" name="kind" value="tipo1" checked>tipo 1</label>
                                    </div>
                                </div> 
                                <div class="col-sm-3" style="margin-top: 0px 0"> 
                                    <div class="radio">
                                      <label><input type="radio" name="kind" value="tipo2">tipo 2</label>
                                    </div>
                                </div> 
                                <div class="col-sm-3" style="margin-top: 0px 0"> 
                                    <div class="radio">
                                      <label><input type="radio" name="kind" value="tipo3">tipo 3</label>
                                    </div> 
                                </div> 
                            </div>
                            <div class="row" style="margin-top:5px; margin-bottom: 30px; margin-left: 0px 0"> 
                                    <div class="col-sm-3"> 
                                    </div>
                                    <div class="col-sm-3"> 
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="submit" class="btn btn-success btn-block">Salvar</button>
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-danger btn-block" data-action="delete-region">Apagar</button>
                                    </div>  
                            </div>
                        </div>     
                        <div class="col-sm-6">  
                            <div class="container" style="width: 490px; margin-top: 0px; margin: auto;"> 
                                <table class="table table-hover">
                                    <thead>
                                        <tr><th style="width: 30%">Usuário</th>
                                            <th style="width: 70%">Comentário</th>
                                        </tr>
                                    </thead> 
                                    <tbody>  
                                    	<tr><td style='width: 30%'>User_0</td><td style='width: 70%'>Aqui, fica o primeiro comentário, abaixo virão as respostas a ele.</td></tr>
                                        <?php                                        	
                                            for ($i=0; $i<10; $i++) { 
                                              echo "<tr><td style='width: 30%'>User_".($i+1)."</td><td style='width: 70%'>Comentário comentário comentário comentário comentário comentário comentário</td></tr>";
                                            }                                            
                                        ?>
                                    </tbody>                
                                </table>
                            </div>
                        </div>
                    </form>           
                </div>
            </div> 
         
            <div class="container">
                <div id="demo">   
                    <div class="row" style="margin: 10px 0">
                        <div class="col-sm-2"> 
                        	<?php 
                        		if ($_SESSION['TIPO'] == 'usuario') {
                        			echo("<button class='btn btn-primary btn-block'
                        					  onclick=navegar('principal_user.php')>
	                                    <span id='Terminar'>
	                                        Concluído
	                                    </span>
	                          		</button>");
                        		}
                        		if ($_SESSION['TIPO'] == 'admin') {
                        			echo("<button class='btn btn-primary btn-block'
                        					  onclick=navegar('principal_admin.php')>
	                                    <span id='Terminar'>
	                                        Concluído
	                                    </span>
	                          		</button>");
                        		}
                        	?> 
                        </div>

                        <!--tirar depois -->
                        <div class="col-sm-2">                             
                        <button class="btn btn-info btn-block" onclick=regionsToDB() title="Export annotations to JSON"><i class="glyphicon glyphicon-file"></i>
                            Export
                        </button>

                        </div>
                    </div>
                </div>
            </div> 
            
            <script>
                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                    ga('create', 'UA-50026819-1', 'wavesurfer.fm');
                    ga('send', 'pageview');  
            </script>
        </div>
    </body>
</html>