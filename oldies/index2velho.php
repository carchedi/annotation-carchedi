<!DOCTYPE html>
<html style="height: 100%;">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <title>Login</title> 
        <script type="text/javascript">
        	alert("login / senha inválidos.");

            function validaForm(nome, senha){
                if (nome == "" ) {
                    alert("Infome o nome de usuário.");
                }else{
                    if (senha == "" ) {
                        alert("Infome sua senha.");
                    }
                } 
            }

            function recuperaSenha(){
                var txt;
                var email = prompt("Infome seu email cadastrado no sistema.");
                if (email) {
                    alert("Uma nova senha será enviada para "+email); 
                } 
            }
        </script>
    </head>
    <body>
        <div class="container text-center" style="background-color:#fff; box-shadow: 0 2px 4px rgba(0,0,0,.15);padding-bottom: 40px; margin-top: 40px; width:600px">
            <h3 style="font-size: 23px">Identifique-se</h3>
            <hr style="width: 500px; border: 0; border-top: 2px solid #204d74;"/>
            <div style="width: 300px; margin-top: 20px; margin: auto" class="text-left">
                <form action="login.php" method="post" name="login_form">
                    <div class="form-group">
                        <label for="matricula">Nome</label>
                        <?php
                        	session_start();
                        	echo "<input type=\"text\" name=\"nome\" id=\"nome\" placeholder=\"nome de usuário\" class=\"form-control\" value=\"".$_SESSION['NOME']."\">";
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="email">Senha</label>
                        <input type="password" name="senha" id="senha" placeholder="senha" class="form-control" autofocus>
                    </div>
                    <div class="text-right" style="margin-top: 20px;">
                        <input type="submit" value="Entrar"
                               onclick="validaForm(nome.value,senha.value)" class="btn btn-primary"/>
                    </div>
                    <div class="text-right" style="margin-top: 5px;">
                        <a style="cursor: pointer" onclick="recuperaSenha()">Esqueci minha senha</a>
                    </div>
                </form>
            </div>
       </div>
        
    </body> 
</html>