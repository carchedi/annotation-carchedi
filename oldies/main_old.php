<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Ferramenta Anotação</title> 

        <!-- Bootstrap -->
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

        <link rel="stylesheet" href="css/style.css"/>
        <link rel="stylesheet" href="css/ribbon.css"/>
        <link rel="stylesheet" href="css/app.css"/>
        <link rel="stylesheet" href="css/teste.css"/>

        <!-- wavesurfer.js -->
        <script src="js/wavesurfer.min.js"></script>

        <!-- plugins -->
        <script src="js/wavesurfer.timeline.min.js"></script>
        <script src="js/wavesurfer.regions.min.js"></script>
        <script src="js/wavesurfer.minimap.min.js"></script>

        <!-- App -->
        <script src="js/trivia.js"></script>
        <script src="js/app_old.js"></script>
    </head>

    <body itemscope itemtype="http://schema.org/WebApplication">
    	<?php
			session_start();
			//Access your POST variables
			$nome = $_SESSION['NOME']; 
			$senha = $_SESSION['SENHA'];  
			if ($nome==null) {
				header('Location: index.html');
			}else{ 
				echo "Usuário: ",$nome,"<br>Senha: ", $senha;
			} 		 
		?>
        <div class="container">
            <div class="header">
                <h1 itemprop="name">Teste - Ferramenta para anotação</h1>
            </div>

            <div id="demo"> 
	        	<div class="col-sm-2">  
                    <label class="btn btn-primary btn-block">
	        		     <input id="audio" type="file" accept="audio/*">  
                         Arquivo
                    </label>
	        		<button class="btn btn-primary btn-block"  onclick="carregar()">
                        <i class="glyphicon glyphicon-file"></i>
                        Carregar
                    </button>	   
                       		  
		        </div>
                <br>
	            <p id="subtitle" class="text-center text-info">&nbsp;</p> 

                <div id="wave-timeline"></div>

                <div id="waveform">
                    <!-- Here be waveform -->
                </div>

                <div class="row" style="margin: 30px 0">
                    <div class="col-sm-2">
                        <button class="btn btn-primary btn-block" data-action="play">
                            <span id="play">
                                <i class="glyphicon glyphicon-play"></i>
                                Play
                            </span>

                            <span id="pause" style="display: none">
                                <i class="glyphicon glyphicon-pause"></i>
                                Pause
                            </span>
                        </button>
                    </div>
                </div>
            </div>

            <form role="form" name="edit" style="opacity: 0; transition: opacity 300ms linear; margin: 30px 0;">

                <div class="row" style="margin: 30px 0">
                    <div class="col-sm-2">
                       <label for="start">Início</label>
                       <input class="form-control" id="start" name="start"/> 
                    </div>
                    <div class="col-sm-2">
                       <label for="end">Fim</label> 
                       <input class="form-control" id="end" name="end"/> 
                    </div>
                </div> 
 
                <div class="form-group">
                    <label for="note">Comentários</label>
                    <textarea id="note" class="form-control" rows="3" name="note"></textarea>
                </div> 

                <div class="form-group">
                	<label>Tipo</label><br>
                		<input type="radio" name="kind" value="tipo1" checked> A &emsp;
					    <input type="radio" name="kind" value="tipo2"> B &emsp;
					    <input type="radio" name="kind" value="tipo3"> C
                </div>

                <div class="row" style="margin: 30px 0">
                    <div class="col-sm-2"> 
                        <button type="submit" class="btn btn-success btn-block">Salvar</button>
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-danger btn-block" data-action="delete-region">Apagar</button>
                    </div>
                </div>
            </form>

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-50026819-1', 'wavesurfer.fm');
            ga('send', 'pageview');
            
            function carregar(){
                //AudioOriginal: http://www.archive.org/download/mshortworks_001_1202_librivox/msw001_03_rashomon_akutagawa_mt_64kb.mp3

                var fileAdress = audio.value;
                fileAdress = fileAdress.replace(/.*[\/\\]/, '');

                wavesurfer.util.ajax({
                    responseType: 'json',
                    url: 'rashomon.json'
                }).on('success', function (data) {
                    wavesurfer.load(
                        //endereço do arquivo:
                        fileAdress,
                        data
                    );
                });
            }
        </script>
    </body>
</html>
