<!DOCTYPE html>
<html>
	<?php
        session_start();
        //Access POST variables
        if( isset($_POST['projeto'])){
        	$projeto = $_POST['projeto'];
        	echo $projeto;
        }
            	
        //Access variables in session
        $nome = $_SESSION['NOME'];
        $senha = $_SESSION['SENHA'];

        if ($nome == null) {
            header('Location: index.html');
        }
    ?>   
<head> 
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/table.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">  
	<link rel="stylesheet" type="text/css" media="all" href="css/daterangepicker.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/> 

	<!--  jQuery -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script> 

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> 


	<style type="text/css">
		#popupbox,#users,#comments,#editarProjeto{
			margin: 0; 
			width: 26%;  
			margin-left:37%; 
			margin-right:37%;
			margin-top: 50px; 
			padding: 10px; 
			position: absolute;  
			border: solid #000000 1px; 
			font-family: arial; 
			visibility: hidden; 
			background-color: #fff;
    		z-index: 15;
		}   

		#divtabela{
			margin: 0; 
			top: 25%;
    		right: 12%;
    		width: 22%;
    		height: 70%;  
			padding: 10px; 
			position: absolute;   
			visibility: hidden; 
			font-family: arial;   
    		z-index: 15;
		}   

      .demo { position: relative; }
      .demo i {
        position: absolute; bottom: 10px; right: 24px; top: auto; cursor: pointer;
      }
    </style>
 	<script src="js/basico.js"></script>
</head>


<body>
	<!--div para que a tabela nao seja acessível enquanto houver algum outro popoup-->
	<div id="divtabela"></div> 

	<div id="users"> 
		<center><b> <?php echo $_POST['projeto-id']; ?> - participantes</b></center>
		<form method="POST" action="acoesDB.php">
			<?php
			echo "<table class='table table-hover' id='tabela'>  
					<tbody style='height: 420px'>";
			for ($i=0; $i < 5; $i++) { 
				echo "<tr><td style='width: 50%'>User_".$i."</td><td style='width: 50%; padding-right: 10%; text-align: right'><input type=\"checkbox\" checked data-toggle=\"toggle\"  data-size=\"mini\"></td></tr>";
			}; 
			echo "</tbody></table>";
			?>  			
		</form>	
		<button class="btn btn-primary btn-block"  onclick="gerenciarUsuarios('hide');" style="width: 30%; margin-top: 5%">
	        <span id="novo">Fechar</span>
	    </button> 	
	</div>

	<div id="comments"> 
		Comentários
		<button class="btn btn-primary btn-block"  onclick="gerenciarComentarios('hide');" style="width: 30%; margin-top: 5%"  >
	        <span id="novo">Fechar</span>
	    </button>   
	</div>

	<div id="editarProjeto">
		<center>
			<b>Editar o projeto</b>
			<form name="edit">
				<table class="table table-bordered" style="margin-top: 20px; margin-bottom: 0px;">
					<tr>
						<td style="text-align:right;width: 20%"><b>Nome</b></td>
						<td style="text-align:left;width: 80%"><input id="nomeEDT" size="14" class="form-control" /></td>
					</tr>  
					<tr>
						<td style="text-align:right;width: 20%"><b>Descrição</b></td>
						<td style="text-align:left;width: 80%"><textarea id="descEDT" class="form-control" rows="4"></textarea> </td>
					</tr>  
					<tr>
						<td style="text-align:right;width: 20%"><b>Período</b></td>
						<td style="text-align:left;width: 55%"> 
							<input type="text" name="daterange" id="periodoEDT" class="form-control"> 
						</td>
					</tr>  
					<tr>
						<td style="text-align:right;width: 20%"><b>Criador</b></td>
						<td style="text-align:left;width: 80%"><input id="criadorEDT" size="14" class="form-control" value=<?php echo $nome; ?> disabled /></td>
					</tr>  
				</table>
			</form>
			<div style="text-align: right;">
				<input type="submit" id="confirma" onclick="submitForm('editar');" class="btn btn-primary"/> 
				<input type="submit" value="Cancelar" onclick="submitForm('cancelar');" class="btn btn-secondary"/> 
			</div> 
		</center>
	</div>

	<div id="popupbox"> 
		<center>			
		<form name="login"> 
			<label id='labelProjeto'><b>Projeto</b></label>
			<table class="table table-bordered" style="margin-top: 20px; margin-bottom: 0px;">
				<tr>
					<td style="text-align:right;width: 20%"><b>Nome</b></td>
					<td style="text-align:left;width: 80%"><input id="nome" size="14" class="form-control" /></td>
				</tr>  
				<tr>
					<td style="text-align:right;width: 20%"><b>Descrição</b></td>
					<td style="text-align:left;width: 80%"><textarea id="desc" class="form-control" rows="4"></textarea> </td>
				</tr>  
				<tr>
					<td style="text-align:right;width: 20%"><b>Período</b></td>
					<td style="text-align:left;width: 55%"> 
						<input type="text" name="daterange" id="periodo" class="form-control"> 
					</td>
				</tr>  
				<tr>
					<td style="text-align:right;width: 20%"><b>Criador</b></td>
					<td style="text-align:left;width: 80%"><input id="criador" size="14" class="form-control" value=<?php echo $nome; ?> disabled /></td>
				</tr>  
			</table>  		
		</form>  		 
			<div style="text-align: right;">
				<input type="submit" id="confirma" onclick="submitForm('criar');" class="btn btn-primary"/> 
				<input type="submit" value="Cancelar" onclick="submitForm('cancelar');" class="btn btn-secondary"/> 
			</div>   					
		</center>  
	</div> 







 
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script> 
    <script type="text/javascript" src="js/moment.min.js"></script>
    <script type="text/javascript" src="js/daterangepicker.js"></script>
    <script type="text/javascript">

    	var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0
		var yyyy = today.getFullYear();

		if(dd<10) {
		    dd = '0'+dd
		} 

		if(mm<10) {
		    mm = '0'+mm
		} 

		today = dd + '/' + mm + '/' + yyyy; 
  
        $('input[name="daterange"]').daterangepicker(
        {
            locale: {
              format: 'DD/MM/YYYY'
            },
            startDate: today,
            endDate: '31/12/'+yyyy
        } );
    </script>


<div class="container text-center" style="background-color:#fff; box-shadow: 0 2px 4px rgba(0,0,0,.15);padding-bottom: 40px; margin-top: 40px; width:90%; min-height: 90%">

    <div class="row" style="margin: 0px"> 
        <div class="col-md-3" style="margin: 0px"></div>
        <div class="col-md-6" style="margin: 0px">
            <h3 style="font-size:23px">Bem-vindo <?php echo $nome; ?></h3>       
    	</div>
    	<div class="col-md-3" style="margin-top: 25px;text-align: right;"> 
    		<a href="logout.php">sair</a> 
    	</div>
    </div>
 

    <hr style="width: 550px; margin-top: 0px; border: 0; bottom:0px; border-top: 2px solid #204d74;"/>
    

    <!--PARTE PRINCIPAL -->
    <div style="width: 90%; margin-top: 20px; margin: auto">
     	<div class="container" style="width: 60%; margin-top: 20px; margin-bottom: 20px;  margin: auto;">
			<label>Projeto:</label>  
        	<form action="principal_admin.php" method="post" id="form1"> 
                <?php 	 
                    include_once 'includes/db_connect.inc.php';   
                    $result = $mysqli->query("SELECT `Nome`, `criador` FROM `projetos` WHERE `finalizado` = 0");  
                    echo "<select name ='projeto-id' class='form-control' id='selectBox' onchange='change()'>";
                    echo "<option value=''></option>";
                    while($row = $result->fetch_assoc()) { 
                        echo "<option value='".$row["Nome"]."'";
                        if (isset($_POST['projeto-id']) && ($row["Nome"] == $_POST['projeto-id'])) {
                            echo "selected";
                        }
                        echo">".$row["Nome"]."</option>";   
                    }  
                    echo "</select>";   
                ?> 
			</form>
		</div>
  
        <div div class="col-sm-4" style="margin-top: 40px;">
        	<b>Ferramentas para os administradores do sistema</b> 
         
            <button id="novoProjeto" class="btn btn-primary btn-block" style="width: 30%; margin-top: 5%" onclick="novoProjeto('show')">
		        <span id="novo">Novo Projeto</span>
		    </button>  
            <button class="btn btn-primary btn-block" style="width: 30%" onclick="" id="estatisticas">
                <span>Estatísticas</span>
            </button>    
            <div style="margin-top: 10%; max-height: 50%">  
            	<?php  
                    echo "<table class='table table-hover'><thead><tr><th align='center' style='width: 100%'>Projetos já Finalizados</th></tr></thead><tbody style='height: 400p'>";

                    include_once 'includes/db_connect.inc.php';   
                    $result = $mysqli->query("SELECT `Nome` FROM `projetos` WHERE `finalizado` = 1");   
                    if ($result->num_rows > 0) {
                    	while($row = $result->fetch_assoc()) { 
                        echo "<tr><td>".$row["Nome"]."</td></tr>";                         
                    	} 
                    }else{
                    	echo "<tr><td>[Aqui ficam os projetos que já tivrem sido finalizados.]</tr></td>";
                    }                    
                    echo "</tbody></table>";                     
            	?>  
            </div>    
        </div>  
        <div div class="col-sm-4">
            <div class="row" style="width: 90%; margin-top: 40px;  margin: auto;">  
            	<div text-align= 'center' style='padding-top: 20px;'>
            		<?php  
            			if (isset($_POST['projeto-id'])) { 
            				$nome = $_POST['projeto-id'];
	            			$esquerda = array('Projeto','Descrição','Criador','Participantes','Início','Término Previsto' ); 
			                $direita = []; 
			                $centro = [];
	            			$result = $mysqli->query("SELECT `idProjeto`, `Nome`, `Descricao`, `Criador`, `Participantes`, `Inicio`, `Termino` FROM `projetos` WHERE `Nome` = '".$_POST['projeto-id']."'"); 
	            			$row = $result->fetch_assoc();  
							$direita[] = $row["Nome"];  
					  		$direita[] = $row["Descricao"];
					  		$direita[] = $row["Criador"];
					  		$direita[] = $row["Participantes"];
					  		$direita[] = $row["Inicio"];
					  		$direita[] = $row["Termino"]; 

					  		//document.getElementById idProjetoH
					  		//document.getElementById criadorH 

					  		echo "<script type='text/javascript'>
					  					var proj_id = '".$row["idProjeto"]."';
					  					var proj_criador = '".$row["Criador"]."';
    					 				var proj_nome = '".$direita[0]."';
    					 				var proj_desc = '".$direita[1]."';
    					 				var proj_inicio = '".$direita[4]."';
    					 				var proje_termino = '".$direita[5]."';
    					 		  </script>";

					  		echo "<table class='table table-bordered' style='margin-top: 20px; margin-bottom: 0px;'>";
					  		for ($i=0; $i < sizeof($direita); $i++) { 
			  					echo "<tr><td style='text-align:right;width: 45%'><b>".$esquerda[$i]."</b></td><td style='text-align:left;width: 55%'>";
			  					 if ($i == 4 || $i == 5) {
			  					 	if (isset($direita[$i])) {
				  					 	$date = explode("-", $direita[$i]);
				  					 	echo $date[2]."/".$date[1]."/".$date[0];
				  					 }
			  					 }else{
			  					 	echo $direita[$i];
			  					 }

			  					echo"</td></tr>";
			  				}
							echo "</table> 
				            </div>
				            <div text-align='center'> 
				            		<button class='btn btn-primary btn-block' onclick=\"editarProjeto('show')\" id='Editar''>
					                    <span>Editar Projeto</span>
					                </button> 
				                	<button class='btn btn-primary btn-block' id='Terminar' onclick=\"submitForm('finalizar')\">
						                <span>Terminar Projeto</span>
						            </button> 
					                <button class='btn btn-primary btn-block' id='Subir' onclick='location.href='''>
					                    <span>Subir Áudios</span>
					                </button> 
					                <button class='btn btn-primary btn-block' id='Adicionar'  onclick=\"gerenciarUsuarios('show')\">
					                    <span>Gerenciar Usuários</span>
					                </button>
					                <button class='btn btn-primary btn-block' id='Comentarios' onclick=\"gerenciarComentarios('show')\">
					                    <span>Gerenciar Comentários</span>
					                </button>   
				            </div>";
						}  			 
            		?> 
            </div>
        </div> 
        <div div class="col-sm-4" >
        	<?php
        		if (isset($_POST['projeto-id'])) { 
					echo "<div class='container' style='width: 90%; margin-top: 40px;' disabled> 
						<table class='table table-hover' id='tabela'> 
							<thead><tr><th style='width: 50%''>Áudio</th><th style='width: 50%; padding-right: 10%; text-align: right'>Comentários</th></tr></thead> 
							<tbody style='height: 420px'>";
					for ($i=0; $i < 200; $i++) { 
						echo "<tr onclick='location.href =\"http://localhost/annotation/annotation_tool.php?id=".$i."\"'; style='cursor: pointer;'><td style='width: 50%'>Áudio_".$i."</td><td style='width: 50%; padding-right: 10%; text-align: right'>".rand(0, 100)."</td></tr>";
					}; 
					echo "</tbody></table></div>";

				}  
			?>
        </div> 
    </div>  

    <form id="escondido" name="escondido" method="post" action="acoesDB.php" hidden>
    	<input type="text" name="idProjetoH" id="idProjetoH" placeholder="id"> 
    	<input type="text" name="nomeH" id="nomeH" placeholder="nome"> 
    	<input type="text" name="descricaoH" id="descricaoH" placeholder="descricao"> 
    	<input type="text" name="periodoH" id="periodoH" placeholder="periodo"> 
    	<input type="text" name="criadorH" id="criadorH" placeholder="criador">
    	<input type="text" name="operacaoH" id="operacaoH" placeholder="operacao">  
    </form>
</div>   
</body>
</html>