<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/table.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <title>Principal</title> 

    <script language='javascript' type='text/javascript'>
        function change(){
		    document.getElementById("form1").submit();
		}
    </script>
</head>
<body>
    <?php
        session_start();
        //Access POST variables
        if( isset($_POST['projeto'])){
        	$projeto = $_POST['projeto'];
        	//echo $projeto;
        }
            	
        //Access variables in session
        $nome = $_SESSION['NOME'];
        $senha = $_SESSION['SENHA'];

        if ($nome == null) {
            header('Location: index.html');
        }

    ?>
<div class="container text-center"
     style="background-color:#fff; box-shadow: 0 2px 4px rgba(0,0,0,.15);padding-bottom: 40px; margin-top: 40px; width:90%; min-height: 90%">

    <div class="row" style="margin: 0px"> 

        <div class="col-md-3" style="margin: 0px"></div>
        <div class="col-md-6" style="margin: 0px">
            <h3 style="font-size:23px">Bem-vindo <?php echo $nome; ?></h3>       
    	</div>
    	<div class="col-md-3" style="margin-top: 25px;text-align: right;"> 
    		<a href="logout.php">sair</a>
    	</div>
    </div>

    <hr style="width: 550px; margin-top: 0px; border: 0; bottom:0px; border-top: 2px solid #204d74;"/>
    <div style="width: 90%; margin-top: 20px; margin: auto">
     	<div class="container" style="width: 60%; margin-top: 20px; margin-bottom: 20px;  margin: auto;">
			<label>Projeto:</label>  
        	<form action="principal_user.php" method="post" id="form1"> 
			  <?php 	 		
			  		include_once 'includes/db_connect.inc.php';	
			  		$result = $mysqli->query("SELECT `Nome` FROM `projetos`"); 
			  		echo "<select name ='projeto-id' class='form-control' id='selectBox' onchange='change()'>";
			  		echo "<option value=''></option>";
			  		while($row = $result->fetch_assoc()) { 
			  			echo "<option value='".$row["Nome"]."'";
                        if (isset($_POST['projeto-id']) && ($row["Nome"] == $_POST['projeto-id'])) {
                            echo "selected";
                        }
                        echo">".$row["Nome"]."</option>"; 
				    }  
			  		echo "</select>"; 
			  ?> 
			</form>
		</div>
  
        <div div class="col-sm-6">
            <div class="row" style="width: 90%; margin-top: 40px;  margin: auto;">  
            	<div text-align= 'center' style='padding-top: 20px;'>
            		<?php
                        $esquerda = array('Projeto','Descrição','Criador','Participantes','Início','Término Previsto' ); 
                        if (isset($_POST['projeto-id'])) {
                            $direita = []; 
                            $result = $mysqli->query("SELECT `Nome`, `Descricao`, `Criador`, `Participantes`, `Inicio`, `Termino` FROM `projetos` WHERE `Nome` = '".$_POST['projeto-id']."'");
                            $row = $result->fetch_assoc();
                            $direita[] = $row["Nome"];  
                            $direita[] = $row["Descricao"];
                            $direita[] = $row["Criador"];
                            $direita[] = $row["Participantes"];
                            $direita[] = $row["Inicio"];
                            $direita[] = $row["Termino"];   
                        } 

                        echo "<table class='table table-bordered' style='margin-top: 20px; margin-bottom: 0px;'>";
                        for ($i=0; $i < sizeof($esquerda); $i++) { 
                            echo "<tr><td style='text-align:right;width: 45%'><b>".$esquerda[$i]."</b></td><td style='text-align:left;width: 55%'>";
                            if (isset($_POST['projeto-id'])) {  
                            	if ($i == 4 || $i == 5) {
                            		$date = explode("-",$direita[$i]);
                            		echo $date[2]."/".$date[1]."/".$date[0];
                            	}else{
	                            	echo $direita[$i];
                            	}              
                            }
                            echo "</td></tr>";
                        }
                        echo "</table></div>";
            		?> 
            </div>
        </div> 
        <div div class="col-sm-6" >
        	<?php
        		if (isset($_POST['projeto-id'])) { 
					echo "<div class='container' style='width: 90%; margin-top: 40px;'> 
						<table class='table table-hover'> 
							<thead><tr><th style='width: 50%''>Áudio</th><th style='width: 50%; padding-right: 10%; text-align: right'>Comentários</th></tr></thead> 
							<tbody style='height: 400p'>";
					for ($i=0; $i < 200; $i++) { 
						echo "<tr onclick='location.href =\"http://localhost/annotation/annotation_tool.php?id=".$i."\"'; style='cursor: pointer;'><td style='width: 50%'>Áudio_".$i."</td><td style='width: 50%; padding-right: 10%; text-align: right'>".rand(0, 100)."</td></tr>";
					}; 
					echo "</tbody></table></div>";
				}
			?>
        </div>  
    </div>
</div>

</body>
<footer style="position: absolute; right: 0;bottom:0;top:100%;left: 0;padding: 1rem;text-align: center;">
    <div class="copyright">
        <div class="container">
            <div class="col-md-12">
                <p></p>
            </div>
        </div>
    </div>
</footer>
</html>