<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/table.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <title>Principal</title> 

    <script language='javascript' type='text/javascript'>
         function annotation(){
         	window.location.href="http://localhost/annotation/annotation_tool.php?id="+Math.floor((Math.random() * 20) + 1); 
         }
    </script>
</head>
<body>
    <?php
        session_start();
        //Access POST variables
        if( isset($_POST['projeto'])){
        	$projeto = $_POST['projeto'];
        	//echo $projeto;
        }
            	
        //Access variables in session
        $nome = $_SESSION['NOME'];
        $senha = $_SESSION['SENHA'];

        if ($nome == null) {
            header('Location: index.html');
        }

    ?>
<div class="container text-center"
     style="background-color:#fff; box-shadow: 0 2px 4px rgba(0,0,0,.15);padding-bottom: 40px; margin-top: 40px; width:90%; min-height: 90%">

    <div class="row" style="margin: 0px"> 

        <div class="col-md-3" style="margin: 0px"></div>
        <div class="col-md-6" style="margin: 0px">
            <h3 style="font-size:23px">Bem-vindo <?php echo $nome; ?></h3>       
    	</div>
    	<div class="col-md-3" style="margin-top: 25px;text-align: right;"> 
    		<a href="logout.php">sair</a>
    	</div>
    </div>

    <hr style="width: 550px; margin-top: 0px; border: 0; bottom:0px; border-top: 2px solid #204d74;"/>
    <div style="width: 90%; margin-top: 20px; margin: auto">
		<div class="text-center" style="margin-top: 20px;">
	   		 <button class="btn btn-primary btn-lg" onclick= annotation()>
	            <span id="play">
	                <h1><i class="glyphicon glyphicon-play"></i><br></h1>
	                <h3>Iniciar Avaliação</h3>
	            </span> 
	        </button>
		</div>
		<br><br>
		<div class="text-center" style="margin-top: 20px;">
	   		 <button class="btn btn-info btn-lg">
	            <span id="play">
	                <h2><i class="glyphicon glyphicon-play"></i><br></h2>
	                Avaliações<br>Incompletas
	            </span> 
	        </button>
		</div>

    </div>
</div>

</body>
<footer style="position: absolute; right: 0;bottom:0;top:100%;left: 0;padding: 1rem;text-align: center;">
    <div class="copyright">
        <div class="container">
            <div class="col-md-12">
                <p></p>
            </div>
        </div>
    </div>
</footer>
</html>