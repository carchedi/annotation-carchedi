<html>
	<head>
	 	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="css/card.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        
        <title>Principal</title>
        <script language='javascript' type='text/javascript'> 
	         function concluido(){
	         	window.location.href="http://localhost/annotation/user_visaoGeral.php";
	         }
		</script>
		
		<style>

			html { height: 90%; }

			.page {
				box-sizing: border-box;
				display: flex;
				flex-direction: column;
				min-width: 800px;
				width: 80%;
				margin: 0 auto;
				height: 100%;
			}
			.tab {
				overflow: hidden;
			}

			.tab button {
				background-color: inherit;
				float: left;
				border: none;
				outline: none;
				padding: 14px 16px;
				transition: 0.3s;
				font-size: 17px;
				
			}

			.tab button:hover {
				background-color: #ddd;
			}

			.tab button.active {
				background-color: #ccc;
			}

			.tabcontent {
				padding: 30px;
				border-top: 1px solid #ccc;
				border-bottom: 1px solid #ccc;
				overflow-y: auto;
			}

		</style>
	</head>
	<body>
		<?php
				session_start();
				//Access POST variables
				if( isset($_POST['projeto'])){
					$projeto = $_POST['projeto'];
					//echo $projeto;
				}
						
				//Access variables in session
				$nome = $_SESSION['NOME'];
				$senha = $_SESSION['SENHA'];

				if ($nome == null) {
					header('Location: index.html');
				}

		?> 	

		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<div class="navbar-brand"> </div>
				</div>
				<ul class="nav navbar-nav navbar-right"> 
					<li><a href="logout.php">Sair</a></li>
				</ul>
			</div>
		</nav> 

		<div class="page" >
			<div class="tab">
			<button class="tablinks" onclick="tab(event, 'Tab1')" id="defaultOpen">Suas Dúvidas Em Aberto</button>
			<button class="tablinks" onclick="tab(event, 'Tab2')">Dúvidas de Outros Avaliadores</button>
			</div>

			<div id="Tab1" class="tabcontent">
				<?php include("user_incompletas_self.php"); ?>
			</div>

			<div id="Tab2" class="tabcontent">
				<?php include("user_incompletas_others.php"); ?>
			</div>

			<script>
			function tab(evt, tab) {
				var i, tabcontent, tablinks;
				tabcontent = document.getElementsByClassName("tabcontent");
				for (i = 0; i < tabcontent.length; i++) {
					tabcontent[i].style.display = "none";
				}
				tablinks = document.getElementsByClassName("tablinks");
				for (i = 0; i < tablinks.length; i++) {
					tablinks[i].className = tablinks[i].className.replace(" active", "");
				}
				document.getElementById(tab).style.display = "block";
				evt.currentTarget.className += " active";
			}

			document.getElementById("defaultOpen").click();
			</script>
			<button class='btn btn-primary' style="margin-top: 20px; width: 200px;" onclick=concluido()>
				Concluído
			</button>
		</div>		
	</body>
</html>