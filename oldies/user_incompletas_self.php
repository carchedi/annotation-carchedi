<html>
	<body>
		<div>
			<div class="card" style="border-color: #BBDEFB">
				<div class="card-container">
					<h4><b>Áudio 1</b></h4>
					<audio controls>
						<source src="I Just Call.mp3" type="audio/mpeg">
						Your browser does not support the audio element.
					</audio>
				</div>
				<div class="card-container">
					<h4><b>Dúvida</b></h4>
						Não consigo entender o que o aluno falou nesta parte
					<h4><b>Resposta</b></h4>
						O aluno diz nesta parte: O poeta é um fingidor. Finge tão completamente Que chega a fingir que é dor A dor que deveras sente.
				</div>
				<div class="card-container" >
					<h4><b>Avaliação Concluida?</b></h4>
					<input type="radio" name="satisfaction" value="satisfactory"> Satisfatório<br>
					<input type="radio" name="satisfaction" value="unsatisfactory"> Insatisfatório<br>
					<button class='btn btn-primary' style="margin: 20px 0;">Finalzar</button>
				</div>
			</div>


			<div class="card" style="border-color: #A5D6A7">
				<div class="card-container">
					<h4><b>Áudio 1</b></h4>
					<audio controls>
						<source src="I Just Call.mp3" type="audio/mpeg">
						Your browser does not support the audio element.
					</audio>
				</div>
				<div class="card-container">
					<h4><b>Dúvida</b></h4>
						Não consigo entender o que o aluno falou nesta parte
					<h4><b>Resposta</b></h4>
						O aluno diz nesta parte: O poeta é um fingidor. Finge tão completamente Que chega a fingir que é dor A dor que deveras sente.
				</div>
				<div class="card-container" >
					<h4><b>Avaliação Concluida?</b></h4>
					<input type="radio" name="satisfaction" value="satisfactory"> Satisfatório<br>
					<input type="radio" name="satisfaction" value="unsatisfactory"> Insatisfatório<br>
					<button class='btn btn-primary'style="margin: 20px 0;">Finalzar</button>
				</div>
			</div>


			<div class="card" style="border-color: #FFF176">
				<div class="card-container">
					<h4><b>Áudio 2</b></h4>
					<audio controls>
						<source src="Just The Way You Are.mp3" type="audio/mpeg">
						Your browser does not support the audio element.
					</audio>
				</div>
				<div class="card-container">
					<h4><b>Dúvida</b></h4>
						Não consigo entender o que o aluno falou nesta parte
					<h4><b>Resposta</b></h4>
						O aluno diz nesta parte: O poeta é um fingidor. Finge tão completamente Que chega a fingir que é dor A dor que deveras sente.
				</div>
				<div class="card-container" >
					<h4><b>Avaliação Concluida?</b></h4>
					<input type="radio" name="satisfaction" value="satisfactory"> Satisfatório<br>
					<input type="radio" name="satisfaction" value="unsatisfactory"> Insatisfatório<br>
					<button class='btn btn-primary'style="margin: 20px 0;">Finalzar</button>
				</div>
			</div>


			<div class="card" style="border-color: #C62828">
				<div class="card-container">
					<h4><b>Áudio 2</b></h4>
					<audio id="audio2" controls ontimeupdate="setMomentaudio(this)">
						<source src="Just The Way You Are.mp3" type="audio/mpeg">
						Your browser does not support the audio element.
					</audio>
				</div>
				<div class="card-container">
					<h4><b>Dúvida</b></h4>
						O som está com muito chiado
					<h4><b>Resposta</b></h4>
						Também não consegui identificar o que foi dito, ruído muito forte.
				</div>
				<div class="card-container" >
					<h4><b>Avaliação Concluida?</b></h4>
					<input type="radio" name="satisfaction" value="satisfactory"> Satisfatório<br>
					<input type="radio" name="satisfaction" value="unsatisfactory"> Insatisfatório<br>
					<button class='btn btn-primary'style="margin: 20px 0;">Finalzar</button>
				</div>
			</div>

			<div class="card" style="border-color: #C62828">
				<div class="card-container">
					<h4><b>Áudio 2</b></h4>
					<audio id="audio2" controls ontimeupdate="setMomentaudio(this)">
						<source src="Just The Way You Are.mp3" type="audio/mpeg">
						Your browser does not support the audio element.
					</audio>
				</div>
				<div class="card-container">
					<h4><b>Dúvida</b></h4>
						O som está com muito chiado
					<h4><b>Resposta</b></h4>
						Também não consegui identificar o que foi dito, ruído muito forte.
				</div>
				<div class="card-container" >
					<h4><b>Avaliação Concluida?</b></h4>
					<input type="radio" name="satisfaction" value="satisfactory"> Satisfatório<br>
					<input type="radio" name="satisfaction" value="unsatisfactory"> Insatisfatório<br>
					<button class='btn btn-primary'style="margin: 20px 0;">Finalzar</button>
				</div>
			</div>
		</div>

		<script>
			var audio = document.getElementById("audio2");
			audio.currentTime = 30;

			function setMomentaudio(audio) {
				if (audio.currentTime > 65) {
					audio.pause();
					audio.currentTime = 30;
				  }
			}		
		</script> 
	</body>
</html>
