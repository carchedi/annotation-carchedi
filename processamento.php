<?php
    include_once 'includes/functions.inc.php';
    include_once 'includes/db_connect.inc.php';
	
    $root = '/home/almeida/Documents/Github/annotation/';

	$input = $root.'audios/input/';
	$output = $root.'audios/output/';

	$db_input = 'audios/input/';
	$db_output = 'audios/output/';

	echo 'Starting...'.rand();

	$files = scandir($input);
	foreach($files as $name) {
		if($name != '..' && $name != '.'){
			$origem = $input.$name;
			echo "File: ".$name.": ".$name."<br>";
			$projeto = intval(explode('_', $name)[0]);
			if(count(explode('_', $name)) > 1){
				// TODO: refatorar trecho de código que faz chamada ao sistema diretamente

				$comando = 'curl -F "project='.$projeto.'" -F "file=@'.$origem.'" http://200.131.219.35:8080/metricas';
				$json = exec($comando);
				$data = json_decode($json);
				//if($data){
					if($data->Oralidade){
						echo 'Oralidade classificada como boa pelo sistema Oralidade<br>';
						$destino = $output.'bom/'.$name;
						if(rename($origem, $destino)){
							echo 'Áudio movido com sucesso<br>';						
							if(criaAudio($db_input.'bom/'.$name, 2, 1, $projeto, $mysqli)){
								echo 'Áudio cadastrado com sucesso<br>';
							}else{
								echo 'Áudio não cadastrado com sucesso<br>';
							}
						}else{
							echo 'Áudio não movido com sucesso<br>';
						}
					}else{
						echo 'Oralidade classificada como ruim pelo sistema Oralidade<br>';
						$destino = $output.'ruim/'.$name;
						if(rename($origem, $destino)){
							echo 'Áudio movido com sucesso<br>';
							if(criaAudio($db_output.'ruim/'.$name, 0, -1, $projeto, $mysqli)){
								echo 'Áudio cadastrado com sucesso<br>';
							}else{
								echo 'Áudio não cadastrado com sucesso<br>';
							}
						}else{
							echo 'Áudio não movido com sucesso<br>';
						}
					}
				//}else{
				//	echo 'Erro na conexão com o servidor';
				//}
			}else{
				echo 'Nome do arquivo é inválido<br>';
			}
		}
	}
	echo 'Fim do processamento<br>';
?>
