 <?php  
	include_once 'includes/functions.inc.php';
	include_once 'includes/db_connect.inc.php';

	if ($_SERVER['REQUEST_METHOD'] === 'GET') {
		if (isset($_GET['idComentario'])){
			if(isset($_GET['resposta'])){
				$resposta = $_GET['resposta'];
				$idComentario = $_GET['idComentario'];

				echo 'Resposta: '.$resposta.'<br>';
				echo 'idComentario: '.$idComentario.'<br>';
				if(criaResposta($resposta, $idComentario, $mysqli)){
					echo 'Resposta cadastrada com sucesso';
				}else{
					echo 'Erro no cadastro da resposta';
				}
			}else{
				$idComentario = $_GET['idComentario'];
				$resposta = recuperaRespostas($idComentario, $mysqli);
				if(count($resposta) > 0){
					header("content-type:application/json");
					echo(json_encode($resposta));
				}else{
					echo '[]';
				}
			}
		}else{
			echo 'Erro: parâmetros de alteração incorretos';
		}
	}