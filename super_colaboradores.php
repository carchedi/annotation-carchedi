<html>
    <?php
        session_start();
        //Access POST variables
        if( isset($_POST['projeto'])){
            $projeto = $_POST['projeto'];
            echo $projeto;
        }
                
        //Access variables in session
        $nome = $_SESSION['NOME'];
        $senha = $_SESSION['SENHA'];

        if ($nome == null) {
            header('Location: index.html');
        }
    ?>   
    <head>
	 	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/sidebar.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <!--Gráfico-->
        <script src="https://cdn.plot.ly/plotly-latest.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/numeric/1.2.6/numeric.min.js"></script>
    </head>
	<body style="background-color: rgb(256,256,256);">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="navbar-brand">Colaboradores</div>
                </div>
                <ul class="nav navbar-nav navbar-right"> 
                    <li><a href="logout.php">Sair</a></li>
                </ul>
            </div>
        </nav>   
        <div style="padding-top: 50px;">
            <div id="wrapper" style="background-color: white">
                <!-- Sidebar -->
                <div id="sidebar-wrapper">
                    <nav id="spy">
                        <ul class="sidebar-nav nav">                     
                            <li>
                                <a href="super_visaoGeral.php" data-scroll>
                                    <span class="fa fa-anchor solo">Visão Geral</span>
                                </a>
                            </li>
                            <li>
                                <a href="super_projetos.php" data-scroll>
                                    <span class="fa fa-anchor solo">Projetos</span>
                                </a> 
                            </li>
                            <li>
                                <a href="super_colaboradores.php" data-scroll>
                                    <span class="fa fa-anchor solo">Colaboradores</span>
                                </a> 
                            </li>
                        </ul>
                    </nav>
                </div>

                <!-- Page content -->
                <div id="page-content-wrapper" style="padding-top: 10px;">
                    <div style="padding: 10px; padding-bottom: 20px;">
                        <center>
	                        <h3 style="font-size:23px">Bem-vindo <?php echo $nome;?></h3>
	                        <hr style="width: 550px; margin-top: 0px; border: 0; bottom:0px; border-top: 2px solid #204d74;"/>
	                        <h4>Colaboradores</h4>  						
                        </center>     
                    </div> 

                    <div class="col-sm-3" style="margin: auto;"> 
                        <button class='btn btn-primary btn-block'>
                            <span>
                                Adicionar Colaborador
                            </span>
                        </button> 
                        <button class='btn btn-primary btn-block'>
                            <span>
                                Mensagem aos Colaboradores
                            </span>
                        </button> 
                        <button class='btn btn-primary btn-block'>
                            <span>
                                Remover Colaborador
                            </span>
                        </button> 
                    </div>
                    <div class="col-sm-4" style="margin: auto;"> 
                        <div id="myDiv3"><!-- Plotly chart will be drawn inside this DIV -->
                          <script>
                            var trace1 = {
                              values:[10, 15, 13, 3],
                              labels: ['Premium','Ouro','Ativo', 'Inativo'], 
                              type: 'pie'
                            }; 
                            var data = [trace1]; 
                            var layout = { 
                                autosize: false,
                                width: 400,
                                height: 300,
                                title: 'Colaboradores - acessos'};
                            Plotly.newPlot('myDiv3', data, layout);
                          </script>
                        </div>
                    </div> 
                    <div class="col-sm-4" style="margin: auto;"> 
                        <div id="myDiv3"><!-- Plotly chart will be drawn inside this DIV -->
                          <script>
                            var trace1 = {
                              values:[10, 15, 13, 3],
                              labels: ['Premium','Ouro','Ativo', 'Inativo'], 
                              type: 'pie'
                            }; 
                            var data = [trace1]; 
                            var layout = { 
                                autosize: false,
                                width: 400,
                                height: 300,
                                title: 'Colaboradores - Avaliações'};
                            Plotly.newPlot('myDiv3', data, layout);
                          </script>
                        </div>
                    </div>       
 
                </div> 
            </div>
        </div> 
    </body> 
</html>
