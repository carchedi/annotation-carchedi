<html>
    <?php
        session_start();
        //Access POST variables
        if( isset($_POST['projeto'])){
            $projeto = $_POST['projeto'];
            echo $projeto;
        }
                
        //Access variables in session
        $nome = $_SESSION['NOME'];
        $senha = $_SESSION['SENHA'];

        if ($nome == null) {
            header('Location: index.html');
        }
    ?>   
    <head>
	 	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/sidebar.css"> 
        <link rel="stylesheet" href="css/table.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <!--Gráfico-->
        <script src="https://cdn.plot.ly/plotly-latest.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/numeric/1.2.6/numeric.min.js"></script>
        <!--datepicker--> 
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>

    </head>
	<body style="background-color: rgb(256,256,256);">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="navbar-brand">Projetos</div>
                </div>
                <ul class="nav navbar-nav navbar-right"> 
                    <li><a href="logout.php">Sair</a></li>
                </ul>
            </div>
        </nav>   
        <div style="padding-top: 50px;">
            <div id="wrapper" style="background-color: white">
                <!-- Sidebar -->
                <div id="sidebar-wrapper">
                    <nav id="spy">
                        <ul class="sidebar-nav nav">                     
                           <li>
                                <a href="super_visaoGeral.php" data-scroll>
                                    <span class="fa fa-anchor solo">Visão Geral</span>
                                </a>
                            </li>
                            <li>
                                <a href="super_projetos.php" data-scroll>
                                    <span class="fa fa-anchor solo">Projetos</span>
                                </a>
                            </li> 
                            <li>
                                <a href="super_colaboradores.php" data-scroll>
                                    <span class="fa fa-anchor solo">Colaboradores</span>
                                </a>
                            </li> 
                        </ul>
                    </nav>
                </div>

                <?php
                    function ping($host, $port){
                        $retorno = false;
                        $waitTimeoutInSeconds = 1; 
                        if($fp = fsockopen($host,$port,$errCode,$errStr,$waitTimeoutInSeconds)){   
                           $retorno = true;
                        }
                        fclose($fp);
                        return $retorno;
                    }
                    echo '<script type="text/javascript">';
                    if(ping('200.131.219.62', 8000)){
                        echo 'alert("Sevidor de cadastro externo encontrado com sucesso. Realize o cadastro externo e preencha em seguida as informações internas.");';
                        echo '$(document).ready(function() {
                            var event = new CustomEvent("abrirCadastroExterno");
                            document.dispatchEvent(event);
                            });';
                    }else{
                        echo 'alert("Sevidor de cadastro externo inalcançável, tente novamente mais tarde ou contacte o administrador do sistema");';
                        echo 'window.location.href = "super_projetos.php";';
                    }
                    echo '</script>';
                ?>

                <script>
                    //Script para chamar janela de cadastro no sistema do João
                    var url = 'http://200.131.219.35/oralidade';
                    document.addEventListener('abrirCadastroExterno', function(){
                        //Abre janela e direciona o foco para ela
                        win = window.open(url + '/projects/create', "janela", "width=1300,height=800,status=yes,scrollbars=yes,resizable=yes");

                        win.focus();

                        //Cria evento que irá reagir quando mensagem for recebida
                        var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
                        var eventer = window[eventMethod];
                        var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
                        eventer(messageEvent,function(e) {

                            //Função callback para quando evento for recebido
                            console.log('Origem: ', e.origin);
                            
                            //confere a origem do evento
                            if( e.origin != url ){ return; }
                            console.log('Mensagem recebida! Dados: ', e.data);
                            window.idExterno.value = e.data.project.id;
                            window.nome.value = e.data.project.name;

                            //Permite edição de campos e exibe mensagem de sucesso
                            window.nome.disabled = false;
                            window.descricao.disabled = false;
                            window.daterange.disabled = false;
                            window.message_1.hidden = true;
                            window.message_2.hidden = false;

                        }, false);

                    });                    
                    
                </script>
                <!-- Page content -->
                <div id="page-content-wrapper" style="padding-top: 10px;">
                    <div style="padding: 10px;">
                        <center>
                            <h3 style="font-size:23px">Bem-vindo <?php echo $nome;?></h3>
                            <hr style="width: 550px; margin-top: 0px; border: 0; bottom:0px; border-top: 2px solid #204d74;"/> 
                        </center>
                        <div class="form-area">  
                            <form class="form-horizontal" role="form" method="POST" action="acoesDB.php">
                             <input type="text" name="operacaoH" value="1" hidden>   
                                <p id = "message_1" style ="text-align: center;" >Passo 1 - Efetue o cadastro do projeto no sistema de avaliação automática de áudios e finalize o cadastro do projeto posteriormente. <br> Caso pop-ups estejam desabilitadas, habilite e atualize a página para que o cadastro possa ser efetuado</p>
                                <p id = "message_2" style ="text-align: center;" hidden>Passo 2 - Prossiga com o cadastro localmente</p>
                            <fieldset> 
                                <legend>Cadastrar um projeto</legend> 

                              <div class="form-group">
                                <label class="col-sm-2 control-label" for="textinput">Nome</label>
                                <div class="col-sm-3">
                                  <input type="text" id = "nome"name="nomeProjeto" placeholder="Nome do projeto" class="form-control" disabled>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-2 control-label" for="textinput">Descrição</label>
                                <div class="col-sm-3">
                                  <textarea name="descricaoProjeto" id = "descricao" placeholder="Descrição" class="form-control" autofocus style="height: 30%; resize:none" disabled></textarea> 
                                </div>
                              </div> 
                              <div class="form-group">
                                <label class="col-sm-2 control-label" for="textinput">Período</label>
                                <div class="col-sm-3">

                                   <input type="text" style="width: 250px; margin-top: 10px;" name="daterange" id='daterange' class="form-control" disabled>
                                    <script>
                                        $(function(){
                                          $('input[name="daterange"]').daterangepicker({
                                              autoUpdateInput: false,
                                              locale: {
                                                  cancelLabel: 'Clear'
                                              } 
                                          });
                                          $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
                                                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));                                                
                                            }
                                          );
                                          $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
                                              $(this).val('');
                                            }
                                          );
                                        });
                                    </script> 
                                </div> 
                              </div>
                              <div class="form-group">
                                <label class="col-sm-2 control-label" for="textinput">IdExterno</label>
                                <div class="col-sm-3">
                                  <input name="idExterno" id = "idExterno" type="text" placeholder="Temporário" class="form-control" readonly>
                                </div> 
                              </div>
                              <div class="form-group">
                                <div class="col-sm-offset-3">
                                  <div class="pull-left">
                                    <button type="submit" class="btn btn-default" formaction="super_projetos.php">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                                  </div>
                                </div>
                              </div>
                            </fieldset>
                          </form>
                        </div>
                    </div>
                </div> 
            </div>
        </div> 
    </body> 
</html>
