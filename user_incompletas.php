<html>
	<?php
        include_once 'includes/functions.inc.php';
        include_once 'includes/db_connect.inc.php';
        session_start();
        //Access POST variables
        if( isset($_POST['projeto'])){
            $projeto = $_POST['projeto'];
            echo $projeto;
        }
                
        //Access variables in session
        $nome = $_SESSION['NOME'];
        $senha = $_SESSION['SENHA'];

        if ($nome == null) {
            header('Location: index.html');
        }	
	        
        function colore(){ 
        	return(rand(0,255).",".rand(0,255).",".rand(0,255)); 
        }

	    ?> 	
	<head>
	 	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/card.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title>Principal</title>
        <script language='javascript' type='text/javascript'>
        	var idComentarioGlobal = 0;

	        function atualiza(id,text,bdid,idAudio){
		      	document.getElementById(id).innerHTML = text;

		      	idComentarioGlobal = bdid;

		      	$("#tbody" + idAudio + " tr").remove();

		      	httpRequest = new XMLHttpRequest();
				if (!httpRequest) {
					alert('Impossível criar uma instância de XMLHttpRequest');
				}
				httpRequest.onreadystatechange = alertContents;
				httpRequest.open('GET', 'respostas.php?idComentario=' + bdid);
				httpRequest.send();

				function alertContents() { 
					if (httpRequest.readyState === XMLHttpRequest.DONE) {
						if (httpRequest.status === 200) { 
							console.log(httpRequest.responseText);
							var parsedJSON = JSON.parse(httpRequest.responseText);
							//alert(parsedJSON);
							if(parsedJSON != undefined){
								//for (var i=parsedJSON.length-1;i>=0;i--) {
								for (var i=0;i<=parsedJSON.length-1;i++) {

				    				var table = document.getElementById("tbody"+parsedJSON[i].idAudio);

								    var row= document.createElement("tr");
								    var td1 = document.createElement("td"); 

								    td1.innerHTML = parsedJSON[i].resposta; 
								    row.appendChild(td1); 
								    table.appendChild(row);
								}
							}
						} else {
							alert('There was a problem with the request.');
						}
					}
				}
	        }

	        function finalizarAudio(audio){
	        	if(document.getElementById('satisf'+audio).checked) {
	        		alert("A avaliação da leitura contida no áudio "+audio+" foi classificada como Satisfatória");
	        		window.location.href = "controle_user_incompletas.php?idAudio="+audio+"&estado=3&resultado=1";
				}else if(document.getElementById('insatisf'+audio).checked) {
	        		alert("A avaliação da leitura contida no áudio "+audio+" foi classificada como Insatisfatória");
	        		window.location.href = "controle_user_incompletas.php?idAudio="+audio+"&estado=3&resultado=0";
				}else{
	        		alert("A avaliação da leitura contida no áudio "+audio+" precisa ser feita para que o áudio possa ser finalizado.");
				}

	        }

	        function navegar(endereço){
                window.location.href=endereço;
            }  

            function add_table_row(id){
			   //"use strict";

				var texto = document.getElementById('campoComentario'+id).value;
			   	var url ='respostas.php?idComentario=' + idComentarioGlobal + '&resposta=' + texto;
				httpRequest = new XMLHttpRequest();
				if (!httpRequest) {
					alert('Não foi possível criar uma instância de XMLHttpRequest');
					return false;
				}
				httpRequest.onreadystatechange = function(){
					console.log(httpRequest.responseText);
				}
				httpRequest.open('GET', url);
				httpRequest.send();

			    var table = document.getElementById("tbody"+id);
			    var row= document.createElement("tr");
			    var td1 = document.createElement("td"); 

			    td1.innerHTML = texto;
			    row.appendChild(td1); 
			    table.appendChild(row);
			    document.getElementById('campoComentario'+id).value = "";
			}
 
 			function enviaComment(id) {
			    var key = window.event.keyCode;

			    // If the user has pressed enter
			    if (key === 13) {
			        add_table_row(id);
			        return false;
			    }else {
			        return true;
			    }
			}

			function ativa_finalizar(){
				document.getElementById("botaoFinalizar").disabled = false;
			}
    	</script>   
    	<style type="text/css">   
    	</style>
	</head>
	<body> 
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <div class="navbar-brand"> </div>
            </div>
            <ul class="nav navbar-nav navbar-right"> 
                <li><a href="logout.php">Sair</a></li>
            </ul>
        </div>
    </nav> 


     <div class="row" style="margin: 0px"> 
        <div class="col-md-3" style="margin: 0px"></div>
        <div class="col-md-6 container text-center"" style="margin: 0px">
            <h3 style="font-size:23px">Bem-vindo <?php echo $nome; ?></h3>
    	</div>
    	<div class="col-md-3" style="margin-top: 25px;text-align: right;">  
    	</div>
    </div>
    <hr style="width: 550px; margin-top: 0px; border: 0; bottom:0px; border-top: 2px solid #204d74;"/>
  	<?php
  	 colore();
  	?>
  	<div class="container">  
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#suas">Suas dúvidas em aberto</a></li>
			<li><a data-toggle="tab" href="#outros">Dúvidas de outros avaliadores</a></li> 
		</ul>

		<div class="tab-content">
			<!--PRIMEIRA ABA-->
			<div id="suas" class="tab-pane fade in active" style=" margin: 0px 0; border: 0px; text-align: justify; text-justify: inter-word; overflow: hidden; overflow-y: scroll; max-height: 55%;">
            	<div class="row"> 
					<?php 
						$audios = recupera_duvidas($_SESSION['NOME'],0,$mysqli);
						for($i=0; $i<sizeof($audios); $i++){
							$coment = recuperaComentarios($audios[$i]['idAudio'],$mysqli);   
							echo "<div class='accordion-body collapse in' id='cartao_".$i."'> 
								<div class=\"card\" style=\"margin:5px;\"> 
									<div class=\"card-container\">
										<h4><b>Áudio ".$audios[$i]['idAudio']."</b></h4>"; 
							for ($j=0; $j < sizeof($coment); $j++) {
								echo "<audio controls onplay=\"atualiza('duvida".$i."','".$coment[$j]['comentario']."',".$coment[$j]['idComentario'].", ".$audios[$i]['idAudio'].")\" controlsList=\"nodownload\" style=\"border-left: 25px solid ".$coment[$j]['cor']."; padding-left: 10px; margin-bottom: 10px;\">
										<source src=\"".$audios[$i]['caminho']."\" type=\"audio/mpeg\">
										Your browser does not support the audio element.
									</audio> 
									";
							} 
											
							echo "<h4><b>Avaliação Concluida?</b></h4>											
											<div class=\"form-check\" onclick=\"ativa_finalizar()\">
										        <input class=\"form-check-input\" name=\"group20".$audios[$i]['idAudio']."\" type=\"radio\" id=\"satisf".$audios[$i]['idAudio']."\" value=1;>
										        <label class=\"form-check-label\" for=\"satisf".$audios[$i]['idAudio']."\">Oralidade satisfatória</label>
										    </div>

										    <div class=\"form-check\" onclick=\"ativa_finalizar()\">
										        <input class=\"form-check-input\" name=\"group20".$audios[$i]['idAudio']."\" type=\"radio\" id=\"insatisf".$audios[$i]['idAudio']."\" value=2;>
										        <label class=\"form-check-label\" for=\"insatisf".$audios[$i]['idAudio']."\">Oralidade insatisfatória</label>
										    </div>

											<button id='botaoFinalizar' class='btn btn-primary' onclick=finalizarAudio(".$audios[$i]['idAudio'].") style=\"margin: 20px 0;\" data-toggle=\"collapse\" data-target=\"#cartao_".$i."\">
												Finalizar
											</button>
 
										</div>

										<div class=\"card-container\">
											<h4><b>Dúvida</b></h4>
											<div id='duvida".$i."'>
												Dê play em um dos áudios para responder a dúvida
											</div>
										</div>

										<div class=\"card-container\"   >
											<table id=\"tabelaRespostas".$audios[$i]['idAudio']."\" style=\"margin-bottom:10px; display: block; height: 30%; width:100%; overflow-y: scroll\" class=\"table\" border=1px>
												<thead><th>Respostas</th></thead>
												<tbody id=\"tbody".$audios[$i]['idAudio']."\">
												</tbody>
												  
											</table>											
                                			<textarea id=\"campoComentario".$audios[$i]['idAudio']."\" style=\"width: 100%; border: 1; resize: none;\" rows=\"3\", placeholder=\"Escreva aqui um comentário...\" onkeypress=\"enviaComment(".$audios[$i]['idAudio'].");\"></textarea> 
											<button class='btn btn-primary pull-right' style=\"margin: 5px 0; text-align: right;\" onclick=\"add_table_row(".$audios[$i]['idAudio'].")\">Responder</button>
										</div>
									</div> 
								 </div>";
						}
                	?>  
            	</div>  
			</div>
			<!--SEGUNDA ABA-->
			<div id="outros" class="tab-pane fade"  style="margin: 0px 0; border: 0px; text-align: justify; text-justify: inter-word; overflow: hidden; overflow-y: scroll; max-height: 55%; min-height: 55%;">
				<div class="row"> 
					<?php 
						$audios = recupera_duvidas($_SESSION['NOME'],1,$mysqli);
						for($i=0; $i<sizeof($audios); $i++){
							$coment = recuperaComentarios($audios[$i]['idAudio'],$mysqli);   
							echo "<div class='accordion-body collapse in' id='cartao_".$i."'> 
								<div class=\"card\" style=\"margin:5px;\"> 
									<div class=\"card-container\">
										<h4><b>Áudio ".$audios[$i]['idAudio']."</b></h4>"; 
							for ($j=0; $j < sizeof($coment); $j++) {
								echo "<audio controls onplay=\"atualiza('duvida_b".$i."','".$coment[$j]['comentario']."',".$coment[$j]['idComentario'].", ".$audios[$i]['idAudio'].")\" controlsList=\"nodownload\" style=\"border-left: 25px solid ".$coment[$j]['cor']."; padding-left: 10px; margin-bottom: 10px;\">
										<source src=\"".$audios[$i]['caminho']."\" type=\"audio/mpeg\">
										Your browser does not support the audio element.
									</audio> 
									";
							} 
											
							echo " </div>
										<div class=\"card-container\">
											<h4><b>Dúvida</b></h4>
											<div id='duvida_b".$i."'>
												Dê play em um dos áudios para responder a dúvida
											</div>
										</div>

										<div class=\"card-container\"   >
											<table id=\"tabelaRespostas".$audios[$i]['idAudio']."\" style=\"margin-bottom:10px; display: block; height: 30%; width:100%; overflow-y: scroll\" class=\"table\" border=1px>
												<thead><tr style=\"width:100%;\"><th>Respostas</th></tr></thead>
												<tbody id=\"tbody".$audios[$i]['idAudio']."\"></tbody>
												  
											</table>											
                                			<textarea id=\"campoComentario".$audios[$i]['idAudio']."\" style=\"width: 100%; border: 1; resize: none;\" rows=\"3\", placeholder=\"Escreva aqui um comentário...\" onkeypress=\"enviaComment(".$audios[$i]['idAudio'].");\"></textarea> 
											<button class='btn btn-primary pull-right' style=\"margin: 5px 0; text-align: right;\" onclick=\"add_table_row(".$audios[$i]['idAudio'].")\">Responder</button>
										</div>
									</div> 
								 </div>";
						}
                	?>  
            	</div> 
			</div>  
		</div>
	</div>  
	<div class="container" style="padding-top: 10px;">
     	<div class="row">
	    	<div class="col-md-2">
				<button class='btn btn-primary btn-block' id='Terminar' onclick=navegar('user_visaoGeral.php')>
			        <span id='Terminar'>
			            Concluído
			        </span>
			    </button>
			</div>
		</div>
	</div>  
 	</body>
</html>
