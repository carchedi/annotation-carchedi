<html>
	<head>
	 	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <title>Principal</title>
        <script language='javascript' type='text/javascript'>
	         function annotation(){
	         	window.location.href="http://localhost/annotation/annotation_tool.php"; 
	         }

	         function incompletas(){
	         	window.location.href="http://localhost/annotation/user_incompletas.php"; 
	         }
    	</script>

        <!--Gráfico-->
        <script src="https://cdn.plot.ly/plotly-latest.min.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/numeric/1.2.6/numeric.min.js"></script>

	</head>
	<body>
    	 <?php
	        session_start();
	        //Access POST variables
	        if( isset($_POST['projeto'])){
	        	$projeto = $_POST['projeto'];
	        	//echo $projeto;
	        }
	            	
	        //Access variables in session
	        $nome = $_SESSION['NOME'];
	        $senha = $_SESSION['SENHA'];

	        if ($nome == null) {
	            header('Location: index.html');
	        }

	    ?> 	

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <div class="navbar-brand"> </div>
            </div>
            <ul class="nav navbar-nav navbar-right"> 
                <li><a href="logout.php">Sair</a></li>
            </ul>
        </div>
    </nav> 


     <div class="row" style="margin: 0px"> 
        <div class="col-md-3" style="margin: 0px"></div>
        <div class="col-md-6 container text-center" style="margin: 0px">
            <h3 style="font-size:23px">Bem-vindo <?php echo $nome; ?></h3>       
    	</div>
    	<div class="col-md-3" style="margin-top: 25px;text-align: right;">  
    	</div>
    </div>
    <hr style="width: 550px; margin-top: 0px; border: 0; bottom:0px; border-top: 2px solid #204d74;"/>

    <div class="d-flex flex-row" style="margin-left:5%;margin-right: 5%;">
    	<div class="col-md-4 container text-center" style="margin: 0px; margin: 0px; padding-top: 8%; visibility: hidden;">
    		 <table class="table table-bordered table-hover" style='margin-top: 20px; margin-bottom: 0px';>
				<tbody>
				<tr><td style="text-align: right;font-weight: bold;">Nome</td><td><?php echo $nome; ?></td></tr>
    			<tr><td style="text-align: right;font-weight: bold;">Ativo desde</td><td>03/03/2018</td></tr>
    			<tr><td style="text-align: right;font-weight: bold;">Categoria</td><td>Ouro</td></tr>
    			<tr><td style="text-align: right;font-weight: bold;">Áudios avaliados</td><td>57</td></tr>
    			<tr><td style="text-align: right;font-weight: bold;">Média semanal</td><td>12.8 áudios</td></tr>
				</tbody> 
			</table>
		</div> 


    	<div class="col-md-4 container text-center"" style="margin: 0px; padding-top: 8%">
			<button class="btn btn-primary btn-lg" onclick= annotation()>
	            <span id="play">
	                <h1><i class="glyphicon glyphicon-play"></i><br></h1>
	                <h3>Iniciar Avaliação</h3>
	            </span> 
	        </button>  
	        <br><br>
	   		<button class="btn btn-info btn-lg" onclick= incompletas()>
	            <span id="play">
	                <h2><i class="glyphicon glyphicon-play"></i><br></h2>
	                Avaliações<br>Incompletas
	            </span> 
	        </button> 	
	    </div>
        <div class="col-md-4 container" style="margin: 0px; visibility: hidden;" >
        	<div id="myDiv"><!-- Plotly chart will be drawn inside this DIV --></div>
	          <script>
	            var trace1 = {
	              x: ['5/Mar','12/Mar','19/Mar','26/Mar','2/Abr','9/Abr','16/Abr','23/Abr'], 
	              y: [10, 15, 13, 17,4,9,7,19], 
	              type: 'bar',
	            }; 
	            var data = [trace1]; 
	            var layout = {
					  autosize: false,
					  width: 500,
					  height: 500,
					  margin: {
					    l: 50,
					    r: 50,
					    b: 100,
					    t: 100,
					    pad: 4
					  },
					  title: 'Acessos - últimas semanas',
					  xaxis: {
					    linecolor: 'grey',
					    linewidth: 1,
					    mirror: true
					  },
					  yaxis: {
					    linecolor: 'grey',
					    linewidth: 1,
					    mirror: true
					  }
					};
	            Plotly.newPlot('myDiv', data, layout);
	          </script>
	        </div>
        </div>
    </div> 
	</body>
</html>
